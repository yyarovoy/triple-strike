#!/bin/bash
DIR=$1
if [ -z $DIR ]; then
	DIR=`pwd`
fi
cd $DIR
C=0
L=0
function sum
{
	N=`wc -l $1 | cut -d' ' -f1`
	C=$(($C + 1))
	L=$(($L + $N))
}
function stat()
{
	HINT=$1
	FILTER=$2

	C=0
	L=0
	for i in $( $FILTER ); do
		sum $i
	done
	echo "$HINT classes:$C lines:$L"
}
stat 'Java:' 'find . -name *.java'
stat 'Flex:' 'find . -name *.as -o -name *.mxml'
stat 'Groovy:' 'find . -name *.groovy'
stat 'GSP:' 'find . -name *.gsp'
stat 'html/js:' 'find . -name *.js -o -name *.htm -o -name *.html'