package ru.s1program.tripleStrike.tasks;

import android.util.Log;
import com.j256.ormlite.dao.Dao;
import org.json.JSONException;
import org.json.JSONObject;
import ru.s1program.tripleStrike.activities.LoginActivity;
import ru.s1program.tripleStrike.orm.DbOpenHelper;
import ru.s1program.tripleStrike.orm.account.Account;
import ru.s1program.tripleStrike.orm.account.Session;
import ru.s1program.tripleStrike.vos.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class LoginTask extends JsonRequestTask<String, Integer>
{

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private String login;
private String password;
private String phone;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public LoginTask(LoginActivity loginActivity)
{
	super(loginActivity);
}

// ----------------------------------------------------------------------
// Protected methods
// ----------------------------------------------------------------------

@Override
protected ActionVO doInBackground(String... params)
{
	login = params[0];
	password = params[1];
	phone = params[2];

	final LoginActionVO actionVO = (LoginActionVO) super.doInBackground(params);

	if (actionVO.getStatus() == 1 && actionVO.getResult() == 1)
	{
		final SessionVO sessionVO = actionVO.getSession();
		final Date now = new Date();

		try
		{
			final DbOpenHelper dbHelper = ownerActivity.getDbHelper();

			final Dao<Account, String> accountDao = dbHelper.getAccountDao();
			final Dao<Session, String> sessionDao = dbHelper.getSessionDao();

			ownerActivity.resetAllSessions();

			final Session session = new Session(sessionVO.getUid(), sessionVO.getSid(), sessionVO.getExpirationDate());

			Account account = accountDao.idExists(actionVO.login) ? accountDao.queryForId(actionVO.login) : new Account(actionVO.login,
			                                                                                                            actionVO.password,
			                                                                                                            actionVO.phone);

			session.active = true;

			account.session = session;
			account.lastSignedIn = now;
			account.lastSignedInServer = now;

			sessionDao.createOrUpdate(session);
			accountDao.createOrUpdate(account);
		}
		catch (SQLException e)
		{
			Log.e(getClass().getName(), "Возникла ошибка при сохранении данных аккаунта на устройстве.", e);

			return failedAction(new ErrorVO(ErrorCode.SQLITE_EXCEPTION,
			                                "Возникла ошибка при сохранении данных аккаунта на устройстве."));
		}
	}

	return actionVO;
}

@Override
protected JSONObject createRequestJson(String... params) throws JSONException
{
	JSONObject jsonObject = new JSONObject();
	JSONObject data = new JSONObject();
	data.put("login", login);
	data.put("password", password);
	data.put("phone", phone);

	JSONObject request = new JSONObject();
	request.put("version", "1.0");
	request.put("action", "login");
	request.put("data", data);

	jsonObject.put("request", request);
	return jsonObject;
}

@Override
protected ActionVO jsonToAction(JSONObject jsonObject, String... params) throws JSONException
{
	final LoginActionVO loginActionVO = new LoginActionVO(jsonObject);
	loginActionVO.login = params[0];
	loginActionVO.password = params[1];
	loginActionVO.phone = params[2];

	return loginActionVO;
}

@Override
protected ActionVO failedAction(ErrorVO errorVO, String... params)
{
	final ArrayList<ErrorVO> errors = new ArrayList<ErrorVO>();
	errors.add(errorVO);

	return new LoginActionVO(login, password, phone, 0, 0, errors);
}

}