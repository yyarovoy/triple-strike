package ru.s1program.tripleStrike.tasks;

import android.util.Log;
import com.j256.ormlite.dao.Dao;
import org.json.JSONException;
import org.json.JSONObject;
import ru.s1program.tripleStrike.activities.ProfileActivity;
import ru.s1program.tripleStrike.orm.DbOpenHelper;
import ru.s1program.tripleStrike.orm.account.Account;
import ru.s1program.tripleStrike.orm.account.Profile;
import ru.s1program.tripleStrike.orm.account.Session;
import ru.s1program.tripleStrike.vos.ActionVO;
import ru.s1program.tripleStrike.vos.ErrorCode;
import ru.s1program.tripleStrike.vos.ErrorVO;
import ru.s1program.tripleStrike.vos.ProfileUpdateActionVO;

import java.sql.SQLException;
import java.util.ArrayList;

public class ProfileUpdateTask extends JsonRequestTask<String, Integer>
{

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private String firstName;
private String lastName;
private String patronymic;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public ProfileUpdateTask(ProfileActivity profileActivity)
{
	super(profileActivity);
}

// ----------------------------------------------------------------------
// Protected methods
// ----------------------------------------------------------------------

@Override
protected ActionVO doInBackground(String... params)
{
	firstName = params[0];
	lastName = params[1];
	patronymic = params[2];

	final ActionVO actionVO = super.doInBackground(params);

	if (actionVO.getStatus() == 1 && actionVO.getResult() == 1)
	{
		try
		{
			final DbOpenHelper dbHelper = ownerActivity.getDbHelper();

			final Dao<Account, String> accountDao = dbHelper.getAccountDao();
			final Dao<Profile, Integer> profileDao = dbHelper.getProfileDao();

			Account account = ownerActivity.resumeAccount();

			Profile profile = (account.profile != null) ? account.profile : new Profile();
			profile.firstName = firstName;
			profile.lastName = lastName;
			profile.patronymic = patronymic;

			account.profile = profile;

			profileDao.createOrUpdate(profile);
			accountDao.update(account);
		}
		catch (SQLException e)
		{
			Log.e(getClass().getName(), "Возникла ошибка при сохранении аккаунта в локальной базе данных: ", e);

			return failedAction(new ErrorVO(ErrorCode.SQLITE_EXCEPTION,
			                                "Возникла ошибка при сохранении данных аккаунта на устройстве."));
		}
	}

	return actionVO;
}

@Override
protected JSONObject createRequestJson(String... params) throws JSONException
{
	Account account = ownerActivity.resumeAccount();
	Session session = account.session;

	final JSONObject jsonObject = new JSONObject();

	final JSONObject jsonSession = new JSONObject();
	jsonSession.put("uid", session.uid);
	jsonSession.put("sid", session.sid);

	final JSONObject data = new JSONObject();
	data.put("firstname", firstName);
	data.put("middlename", patronymic);
	data.put("lastname", lastName);

	final JSONObject request = new JSONObject();
	request.put("version", "1.0");
	request.put("action", "update_profile");
	request.put("session", jsonSession);
	request.put("data", data);

	jsonObject.put("request", request);

	return jsonObject;
}

@Override
protected ActionVO jsonToAction(JSONObject jsonObject, String... params) throws JSONException
{
	final ProfileUpdateActionVO actionVO = new ProfileUpdateActionVO(jsonObject);
	actionVO.firstName = firstName;
	actionVO.lastName = lastName;
	actionVO.patronymic = patronymic;

	return actionVO;
}

@Override
protected ActionVO failedAction(ErrorVO errorVO, String... params)
{
	final ArrayList<ErrorVO> errors = new ArrayList<ErrorVO>();
	errors.add(errorVO);

	return new ProfileUpdateActionVO(firstName, lastName, patronymic, 0, 0, errors);
}

}