package ru.s1program.tripleStrike.tasks;

import android.util.Log;
import com.j256.ormlite.dao.Dao;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.s1program.tripleStrike.activities.RouteActivity;
import ru.s1program.tripleStrike.orm.DbOpenHelper;
import ru.s1program.tripleStrike.orm.account.Account;
import ru.s1program.tripleStrike.orm.account.Session;
import ru.s1program.tripleStrike.orm.route.Route;
import ru.s1program.tripleStrike.orm.route.audit.Audit;
import ru.s1program.tripleStrike.orm.route.audit.Month;
import ru.s1program.tripleStrike.orm.route.audit.MonthPoints;
import ru.s1program.tripleStrike.orm.route.brand.Brand;
import ru.s1program.tripleStrike.orm.route.brand.BrandResult;
import ru.s1program.tripleStrike.orm.route.outlet.Outlet;
import ru.s1program.tripleStrike.orm.route.outlet.OutletType;
import ru.s1program.tripleStrike.orm.route.outlet.RegionKind;
import ru.s1program.tripleStrike.orm.route.unit.Unit;
import ru.s1program.tripleStrike.orm.route.unit.UnitResult;
import ru.s1program.tripleStrike.vos.ActionVO;
import ru.s1program.tripleStrike.vos.ErrorCode;
import ru.s1program.tripleStrike.vos.ErrorVO;
import ru.s1program.tripleStrike.vos.RouteGetActionVO;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class RouteLoadTask extends JsonRequestTask<Void, Integer>
{

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private Session session;

// ----------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------

public RouteLoadTask(RouteActivity ownerActivity)
{
	super(ownerActivity);
}

// ----------------------------------------------------------------------
// Private methods
// ----------------------------------------------------------------------

private Outlet createOutlet(JSONObject jsonTt) throws SQLException, JSONException
{
	return new Outlet(jsonTt.getInt("id"),
	                  jsonTt.getString("name"),
	                  jsonTt.getString("address"),
	                  OutletType.fromCode(jsonTt.getInt("channel_id")),
	                  RegionKind.fromCode(jsonTt.getInt("region_kind")));
}

private Outlet updateOutlet(JSONObject jsonTt) throws SQLException, JSONException
{
	final DbOpenHelper dbHelper = ownerActivity.getDbHelper();
	final Dao<Outlet, Integer> outletDao = dbHelper.getOutletDao();

	Integer unitId = jsonTt.getInt("id");

	Outlet outlet = outletDao.queryForId(unitId);
	outlet.name = jsonTt.getString("name");
	outlet.address = jsonTt.getString("address");
	outlet.region = RegionKind.fromCode(jsonTt.getInt("region_kind"));
	outlet.type = OutletType.fromCode(jsonTt.getInt("channel_id"));

	return outlet;
}

private Audit createAudit(Route route, Outlet outlet, JSONObject jsonTt) throws JSONException, SQLException
{
	if (!jsonTt.has("audit"))
	{
		return null;
	}

	final DbOpenHelper dbHelper = ownerActivity.getDbHelper();
	final Dao<Audit, Integer> auditDao = dbHelper.getAuditDao();
	final Dao<Unit, Integer> unitDao = dbHelper.getUnitDao();
	final Dao<UnitResult, Integer> unitResultDao = dbHelper.getUnitResultDao();
	final Dao<Brand, Integer> brandDao = dbHelper.getBandDao();
	final Dao<BrandResult, Integer> brandResultDao = dbHelper.getBandResultDao();

	JSONObject jsonAudit = jsonTt.getJSONObject("audit");

	Boolean notExist = (jsonAudit.getInt("not_exist") == 1);
	Integer period = jsonAudit.getInt("period");
	Boolean prize = (jsonAudit.getInt("prize") == 1);

	Date startDate = jsonAudit.get("start_date")
	                          .toString()
	                          .equals("null") ? null : new Date(jsonAudit.getLong("start_date") * 1000);
	Date lastUpdated = jsonAudit.get("last_updated")
	                            .toString()
	                            .equals("null") ? null : new Date(jsonAudit.getLong("last_updated") * 1000);

	final Audit audit = new Audit(route, outlet, notExist, period, prize, startDate, lastUpdated);
	auditDao.create(audit);


	JSONArray jsonUnitResults = jsonAudit.getJSONArray("list");

	for (int i = 0; i < jsonUnitResults.length(); i++)
	{
		JSONObject jsonUnitResult = jsonUnitResults.getJSONObject(i);

		Integer jsonUnitId = jsonUnitResult.getInt("id");
		Integer jsonUnitValue = jsonUnitResult.isNull("val") ? null : jsonUnitResult.getInt("val");
		final Unit unit = unitDao.queryForId(jsonUnitId);

		UnitResult unitResult = new UnitResult(audit, unit, jsonUnitValue);
		unitResultDao.create(unitResult);
	}

	JSONArray jsonBrandResults = jsonAudit.getJSONArray("brand");

	for (int j = 0; j < jsonBrandResults.length(); j++)
	{
		JSONObject jsonBrandResult = jsonBrandResults.getJSONObject(j);

		Integer jsonBrandId = jsonBrandResult.getInt("id");
		Integer jsonBrandValue = jsonBrandResult.isNull("val") ? null : jsonBrandResult.getInt("val");
		final Brand brand = brandDao.queryForId(jsonBrandId);

		BrandResult brandResult = new BrandResult(audit, brand, jsonBrandValue);
		brandResultDao.create(brandResult);
	}

	return audit;
}

private Route createRoute(JSONObject jsonRoute) throws SQLException, JSONException
{
	final DbOpenHelper dbHelper = ownerActivity.getDbHelper();

	final Dao<Account, String> accountDao = dbHelper.getAccountDao();
	final Dao<Route, Integer> routeDao = dbHelper.getRouteDao();
	final Dao<Outlet, Integer> outletDao = dbHelper.getOutletDao();
	final Dao<Audit, Integer> auditDao = dbHelper.getAuditDao();

	Integer routeId = jsonRoute.getInt("user_route_id");
	Integer serverRouteId = jsonRoute.getInt("id");
	String routeName = jsonRoute.getString("name");
	Integer routeServerVersion = jsonRoute.getInt("version");

	Route route = new Route(routeId, routeName, routeServerVersion);
	route.serverId = serverRouteId;
	routeDao.create(route);

	JSONObject jsonTtNode = jsonRoute.getJSONObject("tt");

	Iterator ttIterator = jsonTtNode.keys();
	while (ttIterator.hasNext())
	{
		JSONObject jsonTt = jsonTtNode.getJSONObject(ttIterator.next().toString());

		Outlet outlet = outletDao.idExists(jsonTt.getInt("id")) ? updateOutlet(jsonTt) : createOutlet(jsonTt);
		outletDao.createOrUpdate(outlet);

		if (!jsonTt.has("stats"))
		{
			Log.d(getClass().getName(), jsonTt.toString());
			continue;
		}

		JSONArray jsonPoints = jsonTt.getJSONArray("stats");
		createOutletPoints(outlet, jsonPoints);

		createAudit(route, outlet, jsonTt);
	}

	return route;
}

private Route syncRoute(JSONObject jsonRoute)
{
	return null;
}

private void createOutletPoints(Outlet outlet, JSONArray jsonPoints) throws JSONException, SQLException
{
	int p = 0;

	for (int i = 0; i < jsonPoints.length(); i++)
	{
		JSONObject jsonMonth = jsonPoints.getJSONObject(i);

		final Integer mouth = jsonMonth.isNull("month") ? 1 : jsonMonth.getInt("month");
		final Integer firstPoints = jsonMonth.isNull("first_points") ? null : jsonMonth.getInt("first_points");
		final Integer secondPoints = jsonMonth.isNull("second_points") ? null : jsonMonth.getInt("second_points");
		final Integer totalPoints = jsonMonth.isNull("total_points") ? null : jsonMonth.getInt("total_points");
		Boolean prizeGiven = (jsonMonth.getInt("prize") != 0);

		MonthPoints points = new MonthPoints(Month.fromCode(mouth),
		                                     outlet,
		                                     firstPoints,
		                                     secondPoints,
		                                     totalPoints,
		                                     prizeGiven);

		ownerActivity.getDbHelper().getMonthPointsDao().create(points);
	}
}

// ----------------------------------------------------------------------
// Protected methods
// ----------------------------------------------------------------------

@Override
protected JSONObject createRequestJson(Void... params) throws JSONException
{
	final JSONObject jsonObject = new JSONObject();

	final JSONObject jsonSession = new JSONObject();
	jsonSession.put("uid", session.uid);
	jsonSession.put("sid", session.sid);

	final JSONObject request = new JSONObject();
	request.put("version", "1.0");
	request.put("action", "get_route");
	request.put("session", jsonSession);

	jsonObject.put("request", request);

	return jsonObject;
}

@Override
protected ActionVO jsonToAction(JSONObject jsonObject, Void... params) throws JSONException
{
	return new RouteGetActionVO(jsonObject);
}

@Override
protected ActionVO failedAction(ErrorVO errorVO, Void... params)
{
	final ArrayList<ErrorVO> errors = new ArrayList<ErrorVO>();
	errors.add(errorVO);

	return new RouteGetActionVO(0, 0, errors);
}

@Override
protected ActionVO doInBackground(Void... params)
{
	// This calls are necessary for another nested methods of this class.
	Account account = ownerActivity.resumeAccount();
	session = account.session;

	// TODO: Нормальная реализация проверки актуальности сессии.
	if (session == null)
	{
		Log.e(getClass().getName(), ErrorCode.SESSION_EXPIRED.toString());

		return failedAction(new ErrorVO(ErrorCode.SESSION_EXPIRED));
	}

	RouteGetActionVO actionVO = (RouteGetActionVO) super.doInBackground(params);

	try
	{
		// Запрос обработан некорректно.
		if (actionVO.getStatus() == 0)
		{
			Log.e(getClass().getName(), "Запрос обработан с ошибкой.");

			return actionVO;
		}

		// Отрицательный результат обработки запроса.
		if (actionVO.getResult() == 0)
		{
			Log.e(getClass().getName(), "Отрицательный результат обработки запроса.");

			if (actionVO.getErrors() != null && actionVO.getErrors().size() > 0)
			{
				// Закрываем открытые сессии.
				for (ErrorVO errorVO : actionVO.getErrors())
				{
					if (errorVO.code == ErrorCode.SESSION_NOT_EXIST)
					{
						ownerActivity.resetAllSessions();
					}
				}
			}

			return actionVO;
		}

		// Ответ от сервера без данных маршрута.
		if (actionVO.getData() == null || !actionVO.getData().has("route"))
		{
			Log.e(getClass().getName(), "В полученных от сервера данных отсутствует описание маршрута.");

			return failedAction(new ErrorVO(ErrorCode.RESPONSE_ROUTE_DATA_INVALID));
		}

		// Нормальный поток исполнения.

		final DbOpenHelper dbHelper = ownerActivity.getDbHelper();
		final Dao<Route, Integer> routeDao = dbHelper.getRouteDao();
		final Dao<Account, String> accountDao = dbHelper.getAccountDao();

		JSONObject jsonRoute = actionVO.getData().getJSONObject("route");
		Integer routeId = jsonRoute.getInt("user_route_id");


		Route route;
		if (routeDao.idExists(routeId))
		{
			route = syncRoute(jsonRoute);
		}
		else
		{
			route = createRoute(jsonRoute);
		}

		account.route = route;
		accountDao.update(account);

	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Возникла ошибка при сохранении данных маршрута на устройстве.", e);

		return failedAction(new ErrorVO(ErrorCode.SQLITE_EXCEPTION,
		                                "Возникла ошибка при сохранении данных маршрута на устройстве."));
	}
	catch (JSONException e)
	{
		Log.e(getClass().getName(), "Неправильный формат ответа сервера.", e);
		return failedAction(new ErrorVO(ErrorCode.RESPONSE_INVALID));
	}

	return actionVO;
}

}