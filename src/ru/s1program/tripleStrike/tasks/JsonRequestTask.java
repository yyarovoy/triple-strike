package ru.s1program.tripleStrike.tasks;

import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;
import ru.s1program.tripleStrike.activities.TripleStrikeActivity;
import ru.s1program.tripleStrike.net.HttpRequest;
import ru.s1program.tripleStrike.vos.ActionVO;
import ru.s1program.tripleStrike.vos.ErrorCode;
import ru.s1program.tripleStrike.vos.ErrorVO;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public abstract class JsonRequestTask<Params, Progress> extends AsyncTask<Params, Progress, ActionVO>
{

// ----------------------------------------------------------------------
// Protected props
// ----------------------------------------------------------------------

protected TripleStrikeActivity ownerActivity;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

protected JsonRequestTask(TripleStrikeActivity ownerActivity)
{
	this.ownerActivity = ownerActivity;
}

// ----------------------------------------------------------------------
// Private methods
// ----------------------------------------------------------------------

private JSONObject sendRequest(JSONObject requestJson) throws IOException, JSONException
{
	HttpRequest httpRequest = new HttpRequest();
	String responseString = httpRequest.sendJSONPost("http://api.s1program.ru/", requestJson);
	return new JSONObject(responseString).getJSONObject("response");
}

// ----------------------------------------------------------------------
// Protected methods
// ----------------------------------------------------------------------

@Override
protected ActionVO doInBackground(Params... params)
{
	ErrorVO errorVO;

	try
	{
		JSONObject request = createRequestJson(params);
		JSONObject response = sendRequest(request);

		return jsonToAction(response, params);
	}
	catch (JSONException e)
	{
		Log.e(getClass().getName(), "Ошибка при валидации JSON: " + e);
		errorVO = new ErrorVO(ErrorCode.RESPONSE_INVALID);
	}
	catch (UnsupportedEncodingException e)
	{
		Log.e(getClass().getName(), "Неподдерживаемая кодировка JSON: " + e);
		errorVO = new ErrorVO(ErrorCode.RESPONSE_ENCODING_INVALID);
	}
	catch (IOException e)
	{
		Log.e(getClass().getName(), "Ошибка при отправке данных на сервер: " + e);
		errorVO = new ErrorVO(ErrorCode.CONNECTION_NOT_ESTABLISHED);
	}

	return failedAction(errorVO, params);
}

@Override
protected void onPostExecute(ActionVO actionVO)
{
	super.onPostExecute(actionVO);

	ownerActivity.asyncTask_onExecuted(actionVO);
}

protected abstract JSONObject createRequestJson(Params... params) throws JSONException;

protected abstract ActionVO jsonToAction(JSONObject jsonObject, Params... params) throws JSONException;

protected abstract ActionVO failedAction(ErrorVO errorVO, Params... params);

}
