package ru.s1program.tripleStrike.orm.route.unit;

public enum UnitKind
{

// ----------------------------------------------------------------------
// Enumeration
// ----------------------------------------------------------------------

	JUICES(1, "соки"),
	DRINKS(2, "напитки"),
	SNACKS(3, "снеки"),
	NPD(4, "новый продукт (NPD)");

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private int code;
private String description;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

private UnitKind(int code, String description)
{
	this.code = code;
	this.description = description;
}

public int getCode()
{
	return code;
}

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

public static UnitKind fromCode(int code)
{
	for (UnitKind kind : values())
	{
		if (kind.code == code)
		{
			return kind;
		}
	}

	return null;
}

@Override
public String toString()
{
	return description;
}

}