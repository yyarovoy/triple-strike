package ru.s1program.tripleStrike.orm.route.unit;

public enum UnitType
{

// ----------------------------------------------------------------------
// Enumeration
// ----------------------------------------------------------------------

	ASSORTMENT(1, "Ассортимент"),
	EQUIPMENT(2, "Оборудование"),
	ADDITIONAL_EQUIPMENT(3, "Доп. оборудование");

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private final Integer code;
private final String description;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

private UnitType(Integer code, String description)
{
	this.code = code;
	this.description = description;
}

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

public static UnitType fromCode(Integer code)
{
	for (UnitType type : values())
	{
		if (type.code.equals(code))
		{
			return type;
		}
	}

	return null;
}

public int getCode()
{
	return code;
}

@Override
public String toString()
{
	return description;
}

}
