package ru.s1program.tripleStrike.orm.route.unit;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ru.s1program.tripleStrike.orm.route.audit.Audit;

@DatabaseTable(tableName = "unit_result")

public class UnitResult
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

public static final String ID_COLUMN_NAME = "id";
public static final String RESULT_COLUMN_NAME = "result";
public static final String VERSION_COLUMN_NAME = "version";
public static final String AUDIT_COLUMN_NAME = "audit_id";
public static final String UNIT_COLUMN_NAME = "unit_id";

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

@DatabaseField(generatedId = true,
               columnName = ID_COLUMN_NAME)

public Integer id;

@DatabaseField(canBeNull = true,
               columnName = RESULT_COLUMN_NAME)

public Integer result = null;

@DatabaseField(version = true,
               columnName = VERSION_COLUMN_NAME)

public Integer version;

@DatabaseField(foreign = true,
               columnName = AUDIT_COLUMN_NAME)

public Audit audit;

@DatabaseField(canBeNull = false,
               foreign = true,
               foreignAutoRefresh = true,
               columnName = UNIT_COLUMN_NAME)

public Unit unit;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public UnitResult()
{
}

public UnitResult(Audit audit, Unit unit, Integer result)
{
	this.audit = audit;
	this.unit = unit;
	this.result = result;
}

}
