package ru.s1program.tripleStrike.orm.route.unit;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable

/**
 * Единица в ассортименте товара или оборудования.
 */
public class Unit
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

public static final String ID_COLUMN_NAME = "id";
public static final String NAME_COLUMN_NAME = "name";
public static final String KIND_COLUMN_NAME = "kind";
public static final String TYPE_COLUMN_NAME = "type";
public static final String ACTIVE_COLUMN_NAME = "active";

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

@DatabaseField(id = true,
               columnName = ID_COLUMN_NAME)

public int id;

@DatabaseField(canBeNull = false,
               columnName = NAME_COLUMN_NAME)

public String name;

@DatabaseField(canBeNull = false,
               columnName = KIND_COLUMN_NAME)

public UnitKind kind;

@DatabaseField(canBeNull = false,
               columnName = TYPE_COLUMN_NAME)

public UnitType type;

@DatabaseField(canBeNull = false,
               columnName = ACTIVE_COLUMN_NAME)

public Integer active;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public Unit()
{
}

public Unit(int id, String name, UnitType type, UnitKind kind, Integer active)
{
	this.id = id;
	this.name = name;
	this.type = type;
	this.kind = kind;
	this.active = active;
}

}
