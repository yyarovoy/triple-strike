package ru.s1program.tripleStrike.orm.route.unit;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ru.s1program.tripleStrike.orm.route.audit.Audit;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Date;

@DatabaseTable(tableName = "unit_photo")

public class UnitPhoto
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

public static final String ID_COLUMN_NAME = "id";

public static final String FILENAME_COLUMN_NAME = "filename";
public static final String PATH_COLUMN_NAME = "path";

public static final String KIND_COLUMN_NAME = "kind";
public static final String TYPE_COLUMN_NAME = "type";

public static final String SYNCED_COLUMN_NAME = "synced";
public static final String SYNC_CANCELED_COLUMN_NAME = "sync_canceled";
public static final String SYNC_FAILED_COLUMN_NAME = "sync_failed";
public static final String SYNC_FAILURE_COUNT_COLUMN_NAME = "sync_failure_count";

public static final String DATE_CREATED_COLUMN_NAME = "date_created";
public static final String LAST_SYNCED_COLUMN_NAME = "last_synced";
public static final String LAST_UPDATED_COLUMN_NAME = "last_updated";

public static final String LATITUDE_COLUMN_NAME = "latitude";
public static final String LONGITUDE_COLUMN_NAME = "longitude";

public static final String AUDIT_COLUMN_NAME = "audit_id";

// ----------------------------------------------------------------------
// Public props
// ----------------------------------------------------------------------

@DatabaseField(generatedId = true,
               columnName = ID_COLUMN_NAME)

public int id;

@DatabaseField(canBeNull = false,
               columnName = FILENAME_COLUMN_NAME)

public String filename;

@DatabaseField(canBeNull = false,
               columnName = PATH_COLUMN_NAME)

public String path;

@DatabaseField(canBeNull = false,
               columnName = KIND_COLUMN_NAME)

public UnitKind kind;

@DatabaseField(canBeNull = false,
               columnName = TYPE_COLUMN_NAME)

public UnitType type;

@DatabaseField(canBeNull = false,
               columnName = SYNCED_COLUMN_NAME)

public Boolean synced = false;

@DatabaseField(canBeNull = false,
               columnName = SYNC_CANCELED_COLUMN_NAME)

public Boolean syncCanceled = false;

@DatabaseField(canBeNull = false,
               columnName = SYNC_FAILED_COLUMN_NAME)

public Boolean syncFailed = false;

@DatabaseField(canBeNull = false,
               columnName = SYNC_FAILURE_COUNT_COLUMN_NAME)

public Integer syncFailureCount = 0;

@DatabaseField(canBeNull = true,
               columnName = DATE_CREATED_COLUMN_NAME)

public Date dateCreated;

@DatabaseField(canBeNull = true,
               columnName = LAST_SYNCED_COLUMN_NAME)

public Date lastSynced;

@DatabaseField(version = true,
               dataType = DataType.DATE_LONG,
               columnName = LAST_UPDATED_COLUMN_NAME)

public Date lastUpdated;

@DatabaseField(canBeNull = true,
               columnName = LATITUDE_COLUMN_NAME)

public double latitude;

@DatabaseField(canBeNull = true,
               columnName = LONGITUDE_COLUMN_NAME)

public double longitude;

@DatabaseField(foreign = true,
               canBeNull = false,
               columnName = AUDIT_COLUMN_NAME)

public Audit audit;

// ----------------------------------------------------------------------
// Getters and setters
// ----------------------------------------------------------------------

public Bitmap getBitmap() throws FileNotFoundException
{
	FileInputStream stream = new FileInputStream(this.path);
	return BitmapFactory.decodeStream(stream);
}

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public UnitPhoto()
{
}

public UnitPhoto(String filename, String path, Audit audit, UnitKind kind, UnitType type, Boolean synced)
{

	this.filename = filename;
	this.path = path;
	this.audit = audit;
	this.kind = kind;
	this.type = type;
	this.synced = synced;
	this.dateCreated = new Date();
}

}
