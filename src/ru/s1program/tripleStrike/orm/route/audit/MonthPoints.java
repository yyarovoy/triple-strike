package ru.s1program.tripleStrike.orm.route.audit;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ru.s1program.tripleStrike.orm.route.outlet.Outlet;

@DatabaseTable(tableName = "month_points")

public class MonthPoints
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

public static final String ID_COLUMN_NAME = "id";
public static final String MONTH_COLUMN_NAME = "month";
public static final String FIRST_POINTS_COLUMN_NAME = "first_points";
public static final String SECOND_POINTS_COLUMN_NAME = "second_points";
public static final String TOTAL_POINTS_COLUMN_NAME = "total_points";
public static final String PRIZE_GIVEN_COLUMN_NAME = "prize_given";
public static final String OUTLET_COLUMN_NAME = "outlet_id";

// ----------------------------------------------------------------------
// Public props
// ----------------------------------------------------------------------

@DatabaseField(generatedId = true,
               columnName = ID_COLUMN_NAME)

public int id;

@DatabaseField(canBeNull = false,
               columnName = MONTH_COLUMN_NAME)

public Month month;

@DatabaseField(canBeNull = true,
               columnName = FIRST_POINTS_COLUMN_NAME)

public Integer firstPoints;

@DatabaseField(canBeNull = true,
               columnName = SECOND_POINTS_COLUMN_NAME)

public Integer secondPoints;

@DatabaseField(canBeNull = true,
               columnName = TOTAL_POINTS_COLUMN_NAME)

public Integer totalPoints;

@DatabaseField(canBeNull = false,
               columnName = PRIZE_GIVEN_COLUMN_NAME)

public Boolean prizeGiven = false;

@DatabaseField(foreign = true,
               canBeNull = false,
               columnName = OUTLET_COLUMN_NAME)

public Outlet outlet;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public MonthPoints()
{
}

public MonthPoints(Month month,
                   Outlet outlet,
                   Integer firstPoints,
                   Integer secondPoints,
                   Integer totalPoints,
                   Boolean prizeGiven)
{
	this.month = month;
	this.outlet = outlet;
	this.firstPoints = firstPoints;
	this.secondPoints = secondPoints;
	this.totalPoints = totalPoints;
	this.prizeGiven = prizeGiven;
}

}
