package ru.s1program.tripleStrike.orm.route.audit;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import ru.s1program.tripleStrike.orm.route.Route;
import ru.s1program.tripleStrike.orm.route.brand.BrandResult;
import ru.s1program.tripleStrike.orm.route.outlet.Outlet;
import ru.s1program.tripleStrike.orm.route.unit.UnitPhoto;
import ru.s1program.tripleStrike.orm.route.unit.UnitResult;

import java.util.Date;

@DatabaseTable

/**
 * Проверка конкретной торговой точки аудитором.
 */
public class Audit
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

public static final String ID_FIELD_NAME = "id";
public static final String NOT_EXIST_FIELD_NAME = "not_exist";
public static final String PERIOD_FIELD_NAME = "period";
public static final String PRIZE_FIELD_NAME = "prize";
public static final String ROUTE_FIELD_NAME = "route_id";
public static final String OUTLET_FIELD_NAME = "outlet_id";

public static final String START_DATE_FIELD_NAME = "start_date";
public static final String LAST_SYNCED_COLUMN_NAME = "last_synced";
public static final String LAST_UPDATED = "last_updated";

public static final String VERSION_FIELD_NAME = "version";

public static final String SYNCED_COLUMN_NAME = "synced";
public static final String SYNC_CANCELED_COLUMN_NAME = "sync_canceled";
public static final String SYNC_FAILED_COLUMN_NAME = "sync_failed";
public static final String SYNC_FAILURE_COUNT_COLUMN_NAME = "sync_failure_count";

// ----------------------------------------------------------------------
// Public props
// ----------------------------------------------------------------------

@DatabaseField(generatedId = true,
               columnName = ID_FIELD_NAME)

public Integer id;

@DatabaseField(canBeNull = false,
               foreign = true,
               columnName = ROUTE_FIELD_NAME)

public Route route;

@DatabaseField(canBeNull = false,
               foreign = true,
               foreignAutoRefresh = true,
               columnName = OUTLET_FIELD_NAME)

public Outlet outlet;

@ForeignCollectionField

public ForeignCollection<UnitResult> unitResults;

@ForeignCollectionField

public ForeignCollection<BrandResult> brandResults;

@ForeignCollectionField

public ForeignCollection<AuditPhoto> auditPhotos;

@ForeignCollectionField

public ForeignCollection<UnitPhoto> unitPhotos;

@DatabaseField(canBeNull = false,
               columnName = NOT_EXIST_FIELD_NAME)

public Boolean notExist = false;

@DatabaseField(canBeNull = false,
               columnName = PERIOD_FIELD_NAME)

public Integer period = 0;

@DatabaseField(canBeNull = false,
               columnName = PRIZE_FIELD_NAME)

public Boolean prize = false;

@DatabaseField(canBeNull = true,
               columnName = START_DATE_FIELD_NAME)

public Date startDate;

@DatabaseField(canBeNull = true,
               columnName = LAST_SYNCED_COLUMN_NAME)

public Date lastSynced;

@DatabaseField(version = true,
               dataType = DataType.DATE_LONG,
               canBeNull = true, columnName = LAST_UPDATED)

public Date lastUpdated;

@DatabaseField(version = true,
               columnName = VERSION_FIELD_NAME)

public Integer version;

@DatabaseField(canBeNull = false,
               columnName = SYNCED_COLUMN_NAME)

public Boolean synced = true;

@DatabaseField(canBeNull = false,
               columnName = SYNC_CANCELED_COLUMN_NAME)

public Boolean syncCanceled = false;

@DatabaseField(canBeNull = false,
               columnName = SYNC_FAILED_COLUMN_NAME)

public Boolean syncFailed = false;

@DatabaseField(canBeNull = false,
               columnName = SYNC_FAILURE_COUNT_COLUMN_NAME)

public Integer syncFailureCount = 0;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public Audit()
{
}

public Audit(Outlet outlet, Boolean notExist, Integer period, Boolean prize, Date startDate, Date lastUpdated)
{
	this.outlet = outlet;
	this.notExist = notExist;
	this.period = period;
	this.prize = prize;
	this.startDate = startDate;
	this.lastUpdated = lastUpdated;
}

public Audit(Route route,
             Outlet outlet,
             Boolean notExist,
             Integer period,
             Boolean prize,
             Date startDate,
             Date lastUpdated)
{
	this.route = route;
	this.outlet = outlet;
	this.notExist = notExist;
	this.period = period;
	this.prize = prize;
	this.startDate = startDate;
	this.lastUpdated = lastUpdated;
}

public Audit(Route route,
             Outlet outlet,
             Boolean notExist,
             Integer period,
             Boolean prize,
             Boolean synced,
             Date startDate,
             Date lastUpdated,
             Integer version)
{
	this.route = route;
	this.outlet = outlet;
	this.notExist = notExist;
	this.period = period;
	this.prize = prize;
	this.synced = synced;
	this.startDate = startDate;
	this.lastUpdated = lastUpdated;
	this.version = version;
}

}
