package ru.s1program.tripleStrike.orm.route.audit;

public enum Month
{

// ----------------------------------------------------------------------
// Enumeration
// ----------------------------------------------------------------------

	JANUARY(1, "Январь"),
	FEBRUARY(2, "Февраль"),
	MARCH(3, "Март"),
	APRIL(4, "Апрель"),
	MAY(5, "Май"),
	JUNE(6, "Июнь"),
	JULY(7, "Июль"),
	AUGUST(8, "Август"),
	SEPTEMBER(9, "Сентябрь"),
	OCTOBER(10, "Октябрь"),
	NOVEMBER(11, "Ноябрь"),
	DECEMBER(12, "Декабрь");

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private final Integer code;
private final String description;

// ----------------------------------------------------------------------
// Getters and setters
// ----------------------------------------------------------------------

public Integer getCode()
{
	return code;
}

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

private Month(Integer code, String description)
{
	this.code = code;
	this.description = description;
}

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

public static Month fromCode(Integer code)
{
	for (Month month : values())
	{
		if (month.getCode().equals(code))
		{
			return month;
		}
	}

	return null;
}

@Override
public String toString()
{
	return description;
}

}
