package ru.s1program.tripleStrike.orm.route.brand;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable

/**
 * Брэнд продукции, посдсчет которой необходимо провести.
 */
public class Brand
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

public static final String ID_COLUMN_NAME = "id";
public static final String NAME_COLUMN_NAME = "name";

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

@DatabaseField(id = true,
               columnName = ID_COLUMN_NAME)

public int id;

@DatabaseField(canBeNull = false,
               columnName = NAME_COLUMN_NAME)

public String name;

// ----------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------

public Brand()
{
}

public Brand(int id, String name)
{
	this.id = id;
	this.name = name;
}

}
