package ru.s1program.tripleStrike.orm.route.brand;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ru.s1program.tripleStrike.orm.route.audit.Audit;

@DatabaseTable(tableName = "brand_result")

/**
 * Результат проверки брэнда
 */
public class BrandResult
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

public static final String ID_COLUMN_NAME = "id";
public static final String RESULT_COLUMN_NAME = "result";
public static final String VERSION_COLUMN_NAME = "version";
public static final String AUDIT_COLUMN_NAME = "audit_id";
public static final String BRAND_COLUMN_NAME = "brand_id";

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

@DatabaseField(generatedId = true,
               columnName = ID_COLUMN_NAME)

public Integer id;

@DatabaseField(foreign = true,
               columnName = AUDIT_COLUMN_NAME)

public Audit audit;

@DatabaseField(version = true,
               columnName = VERSION_COLUMN_NAME)

public Integer version;


@DatabaseField(canBeNull = false,
               foreign = true,
               foreignAutoRefresh = true,
               columnName = BRAND_COLUMN_NAME)

public Brand brand;

@DatabaseField(canBeNull = true,
               columnName = RESULT_COLUMN_NAME)

public Integer result;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public BrandResult()
{
}

public BrandResult(Audit audit, Brand brand, Integer result)
{
	this.audit = audit;
	this.brand = brand;
	this.result = result;
}

}
