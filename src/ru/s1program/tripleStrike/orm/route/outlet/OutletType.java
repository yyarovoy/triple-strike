package ru.s1program.tripleStrike.orm.route.outlet;

/**
 * Тип торговой точки.
 */
public enum OutletType
{

// ----------------------------------------------------------------------
// Enumeration
// ----------------------------------------------------------------------

	UNKNOWN(0),
	MINI_MARKET(1),
	MARKET(2);

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private Integer code;

// ----------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------

private OutletType(Integer code)
{
	this.code = code;
}

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

public static OutletType fromCode(int code)
{
	for (OutletType type : values())
	{
		if (type.code == code)
		{
			return type;
		}
	}

	return null;
}

}
