package ru.s1program.tripleStrike.orm.route.outlet;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import ru.s1program.tripleStrike.orm.route.audit.MonthPoints;

@DatabaseTable
public class Outlet
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

public static final String ID_COLUMN_NAME = "id";
public static final String NAME_COLUMN_NAME = "name";
public static final String ADDRESS_COLUMN_NAME = "address";
public static final String REGION_COLUMN_NAME = "region";
public static final String TYPE_COLUMN_NAME = "type";

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

@DatabaseField(id = true,
               columnName = ID_COLUMN_NAME)

public Integer id;

@DatabaseField(canBeNull = false,
               columnName = NAME_COLUMN_NAME)

public String name;

@DatabaseField(canBeNull = false,
               columnName = ADDRESS_COLUMN_NAME)

public String address;

@DatabaseField(canBeNull = false,
               columnName = REGION_COLUMN_NAME)

public RegionKind region = RegionKind.UNKNOWN;

@DatabaseField(canBeNull = false,
               columnName = TYPE_COLUMN_NAME)

public OutletType type = OutletType.UNKNOWN;

@ForeignCollectionField

public ForeignCollection<MonthPoints> points;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public Outlet()
{
}

public Outlet(Integer id, String name, String address, OutletType type, RegionKind region)
{
	this.id = id;
	this.name = name;
	this.address = address;
	this.type = type;
	this.region = region;
}

}
