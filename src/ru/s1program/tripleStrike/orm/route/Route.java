package ru.s1program.tripleStrike.orm.route;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import ru.s1program.tripleStrike.orm.route.audit.Audit;

@DatabaseTable

public class Route
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

public static final String ID_COLUMN_NAME = "id";
public static final String SERVER_ID_COLUMN_NAME = "server_id";
public static final String NAME_COLUMN_NAME = "name";
public static final String VERSION_COLUMN_NAME = "version";
public static final String SERVER_VERSION_COLUMN_NAME = "server_version";

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

@DatabaseField(id = true,
               columnName = ID_COLUMN_NAME)

public Integer id;

@DatabaseField(canBeNull = false,
               columnName = SERVER_ID_COLUMN_NAME)

public Integer serverId;

@DatabaseField(canBeNull = false,
               columnName = NAME_COLUMN_NAME)

public String name;

@ForeignCollectionField

public ForeignCollection<Audit> audits;

@DatabaseField(version = true,
               columnName = VERSION_COLUMN_NAME)

public Integer version;

@DatabaseField(canBeNull = false,
               columnName = SERVER_VERSION_COLUMN_NAME)

public Integer serverVersion = 0;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public Route()
{
}

public Route(Integer id, String name, Integer serverVersion)
{
	this.id = id;
	this.name = name;
	this.serverVersion = serverVersion;
}

}
