package ru.s1program.tripleStrike.orm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.csvreader.CsvReader;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import ru.s1program.tripleStrike.R;
import ru.s1program.tripleStrike.orm.account.Account;
import ru.s1program.tripleStrike.orm.account.Profile;
import ru.s1program.tripleStrike.orm.account.Session;
import ru.s1program.tripleStrike.orm.route.Route;
import ru.s1program.tripleStrike.orm.route.audit.Audit;
import ru.s1program.tripleStrike.orm.route.audit.AuditPhoto;
import ru.s1program.tripleStrike.orm.route.audit.MonthPoints;
import ru.s1program.tripleStrike.orm.route.brand.Brand;
import ru.s1program.tripleStrike.orm.route.brand.BrandResult;
import ru.s1program.tripleStrike.orm.route.outlet.Outlet;
import ru.s1program.tripleStrike.orm.route.unit.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.Random;

public class DbOpenHelper extends OrmLiteSqliteOpenHelper
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

private static final String DATABASE_NAME = "main.db";
private static final int DATABASE_VERSION = 2;

//private static final String DATABASE_NAME = "test2.db";
//private static final int DATABASE_VERSION = 2;
//private static final int DATABASE_VERSION = new Random().nextInt(100);

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private Dao<Account, String> accountDao;
private Dao<Session, String> sessionDao;
private Dao<Profile, Integer> profileDao;
private Dao<Route, Integer> routeDao;
private Dao<Audit, Integer> auditDao;
private Dao<AuditPhoto, Integer> auditPhotoDao;
private Dao<Outlet, Integer> outletDao;
private Dao<Unit, Integer> unitDao;
private Dao<UnitResult, Integer> unitResultDao;
private Dao<UnitPhoto, Integer> unitPhotoDao;
private Dao<Brand, Integer> brandDao;
private Dao<BrandResult, Integer> brandResultDao;
private Dao<MonthPoints, Integer> monthPointsDao;

private Context context;

// ----------------------------------------------------------------------
// Getters and setters
// ----------------------------------------------------------------------

public Dao<Account, String> getAccountDao() throws SQLException
{
	if (accountDao == null)
	{
		accountDao = getDao(Account.class);
	}
	return accountDao;
}

public Dao<Session, String> getSessionDao() throws SQLException
{
	if (sessionDao == null)
	{
		sessionDao = getDao(Session.class);
	}
	return sessionDao;
}

public Dao<Profile, Integer> getProfileDao() throws SQLException
{
	if (profileDao == null)
	{
		profileDao = getDao(Profile.class);
	}
	return profileDao;
}

public Dao<Route, Integer> getRouteDao() throws SQLException
{
	if (routeDao == null)
	{
		routeDao = getDao(Route.class);
	}
	return routeDao;
}

public Dao<Audit, Integer> getAuditDao() throws SQLException
{
	if (auditDao == null)
	{
		auditDao = getDao(Audit.class);
	}
	return auditDao;
}

public Dao<AuditPhoto, Integer> getAuditPhotoDao() throws SQLException
{
	if (auditPhotoDao == null)
	{
		auditPhotoDao = getDao(AuditPhoto.class);
	}
	return auditPhotoDao;
}

public Dao<Outlet, Integer> getOutletDao() throws SQLException
{
	if (outletDao == null)
	{
		outletDao = getDao(Outlet.class);
	}
	return outletDao;
}

public Dao<Unit, Integer> getUnitDao() throws SQLException
{
	if (unitDao == null)
	{
		unitDao = getDao(Unit.class);
	}
	return unitDao;
}

public Dao<UnitResult, Integer> getUnitResultDao() throws SQLException
{
	if (unitResultDao == null)
	{
		unitResultDao = getDao(UnitResult.class);
	}
	return unitResultDao;
}

public Dao<UnitPhoto, Integer> getUnitPhotoDao() throws SQLException
{
	if (unitPhotoDao == null)
	{
		unitPhotoDao = getDao(UnitPhoto.class);
	}
	return unitPhotoDao;
}

public Dao<Brand, Integer> getBandDao() throws SQLException
{
	if (brandDao == null)
	{
		brandDao = getDao(Brand.class);
	}
	return brandDao;
}

public Dao<BrandResult, Integer> getBandResultDao() throws SQLException
{
	if (brandResultDao == null)
	{
		brandResultDao = getDao(BrandResult.class);
	}
	return brandResultDao;
}

public Dao<MonthPoints, Integer> getMonthPointsDao() throws SQLException
{
	if (monthPointsDao == null)
	{
		monthPointsDao = getDao(MonthPoints.class);
	}
	return monthPointsDao;
}

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public DbOpenHelper(Context context)
{
	super(context, DATABASE_NAME, null, DATABASE_VERSION);

	this.context = context;
}

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

@Override
public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource)
{
	try
	{
		TableUtils.createTableIfNotExists(connectionSource, Session.class);
		TableUtils.createTableIfNotExists(connectionSource, Profile.class);
		TableUtils.createTableIfNotExists(connectionSource, Account.class);
		TableUtils.createTableIfNotExists(connectionSource, Unit.class);
		TableUtils.createTableIfNotExists(connectionSource, UnitResult.class);
		TableUtils.createTableIfNotExists(connectionSource, Brand.class);
		TableUtils.createTableIfNotExists(connectionSource, BrandResult.class);
		TableUtils.createTableIfNotExists(connectionSource, Outlet.class);
		TableUtils.createTableIfNotExists(connectionSource, Audit.class);
		TableUtils.createTableIfNotExists(connectionSource, Route.class);
		TableUtils.createTableIfNotExists(connectionSource, AuditPhoto.class);
		TableUtils.createTableIfNotExists(connectionSource, UnitPhoto.class);
		TableUtils.createTableIfNotExists(connectionSource, MonthPoints.class);

		// Наполняем таблицу ассортимента данными.
		final Dao<Unit, Integer> unitDao = getUnitDao();
		if (unitDao.countOf() == 0)
		{
			Log.d(getClass().getName(), "Наполняем таблицу ассортимента данными.");

			try
			{

				InputStream inputStream = context.getResources().openRawResource(R.raw.audit_list);

				CsvReader units = new CsvReader(inputStream, Charset.forName("UTF-8"));
				units.readHeaders();

				while (units.readRecord())
				{
					int unitId = Integer.parseInt(units.get("item_id"));
					String unitName = units.get("name");
					int unitTypeCode = Integer.parseInt(units.get("type"));
					int unitKindCode = Integer.parseInt(units.get("kind"));
					int unitActive = Integer.parseInt(units.get("active"));

					UnitType unitType = UnitType.fromCode(unitTypeCode);
					UnitKind unitKind = UnitKind.fromCode(unitKindCode);

					//				Log.d(getClass().getName(), "Сохраняю в базе элемент " + unitId + " " + unitName + " " + unitType + " " + unitKind + ".");
					unitDao.create(new Unit(unitId, unitName, unitType, unitKind, unitActive));
				}

				units.close();
			}
			catch (FileNotFoundException e)
			{
				Log.e(getClass().getName(), "Не найден инициализационный файл со списком SKU.", e);
			}
			catch (IOException e)
			{
				Log.e(getClass().getName(), "Ошибка импорта списка SKU из инициализационного файла.", e);
			}
		}

		// Наполняем таблицу брэндов данными.
		final Dao<Brand, Integer> brandDao = getBandDao();
		if (brandDao.countOf() == 0)
		{
			Log.d(getClass().getName(), "Наполняем таблицу брэндов данными.");

			try
			{
				InputStream brandStream = context.getResources().openRawResource(R.raw.audit_brand);

				CsvReader brands = new CsvReader(brandStream, Charset.forName("UTF-8"));
				brands.readHeaders();

				while (brands.readRecord())
				{
					int brandId = Integer.parseInt(brands.get("brand_id"));
					String brandName = brands.get("name");

					//				Log.d(getClass().getName(), "Сохраняю в базе брэнд [" + brandId + "] " + brandName + ".");
					brandDao.create(new Brand(brandId, brandName));
				}

				brands.close();
			}
			catch (FileNotFoundException e)
			{
				Log.e(getClass().getName(), "Не найден инициализационный файл со списком брэндов.", e);
			}
			catch (IOException e)
			{
				Log.e(getClass().getName(), "Ошибка импорта списка брэндов из инициализационного файла.", e);
			}
		}
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Возникла ошибка во время создания таблиц в базе данных.", e);
	}
}

@Override
public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVer, int newVer)
{
	/*try
	{
		TableUtils.dropTable(connectionSource, Session.class, true);
		TableUtils.dropTable(connectionSource, Profile.class, true);
		TableUtils.dropTable(connectionSource, Account.class, true);
		TableUtils.dropTable(connectionSource, Unit.class, true);
		TableUtils.dropTable(connectionSource, UnitResult.class, true);
		TableUtils.dropTable(connectionSource, Brand.class, true);
		TableUtils.dropTable(connectionSource, BrandResult.class, true);
		TableUtils.dropTable(connectionSource, Outlet.class, true);
		TableUtils.dropTable(connectionSource, Audit.class, true);
		TableUtils.dropTable(connectionSource, Route.class, true);
		TableUtils.dropTable(connectionSource, AuditPhoto.class, true);
		TableUtils.dropTable(connectionSource, UnitPhoto.class, true);
		TableUtils.dropTable(connectionSource, MonthPoints.class, true);

		onCreate(sqLiteDatabase, connectionSource);

		Log.d(getClass().getName(), "Database recreated.");
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Unable to upgrade database from version " + oldVer + " to new " + newVer, e);
	}*/
}

}
