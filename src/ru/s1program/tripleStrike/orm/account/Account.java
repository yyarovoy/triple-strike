package ru.s1program.tripleStrike.orm.account;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import ru.s1program.tripleStrike.orm.route.Route;

import java.util.Date;

@DatabaseTable

/**
 * Аккаунт пользователя в системе.
 */
public class Account
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

public static final String LOGIN_COLUMN_NAME = "login";
public static final String PASSWORD_COLUMN_NAME = "password";
public static final String PHONE_COLUMN_NAME = "phone";
public static final String VERSION_COLUMN_NAME = "version";
public static final String LAST_SIGNED_IN_COLUMN_NAME = "last_signed_in";
public static final String LAST_SIGNED_IN_SERVER_COLUMN_NAME = "last_signed_in_server";
public static final String PROFILE_COLUMN_NAME = "profile_id";
public static final String SESSION_COLUMN_NAME = "session_id";
public static final String ROUTE_COLUMN_NAME = "rout_id";

// ----------------------------------------------------------------------
// Public props
// ----------------------------------------------------------------------

@DatabaseField(id = true,
               width = 10,
               columnName = LOGIN_COLUMN_NAME)

public String login;

@DatabaseField(canBeNull = false,
               width = 6,
               columnName = PASSWORD_COLUMN_NAME)

public String password;

@DatabaseField(canBeNull = false,
               width = 11,
               columnName = PHONE_COLUMN_NAME)

public String phone;

@DatabaseField(columnName = LAST_SIGNED_IN_COLUMN_NAME)

public Date lastSignedIn;

@DatabaseField(columnName = LAST_SIGNED_IN_SERVER_COLUMN_NAME)

public Date lastSignedInServer;

@DatabaseField(version = true,
               columnName = VERSION_COLUMN_NAME)

public Integer version;

@DatabaseField(canBeNull = true,
               foreign = true,
               foreignAutoCreate = true,
               foreignAutoRefresh = true,
               columnName = PROFILE_COLUMN_NAME)

public Profile profile;

@DatabaseField(foreign = true,
               foreignAutoCreate = true,
               foreignAutoRefresh = true,
               columnName = SESSION_COLUMN_NAME)

public Session session;

@DatabaseField(foreign = true,
               foreignAutoCreate = true,
               foreignAutoRefresh = true,
               maxForeignAutoRefreshLevel = 1,
               columnName = ROUTE_COLUMN_NAME)

public Route route;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public Account()
{
}

public Account(String login, String password, String phone)
{
	this.login = login;
	this.password = password;
	this.phone = phone;
}

}
