package ru.s1program.tripleStrike.orm.account;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

@DatabaseTable

/**
 * Сессия пользователя.
 */
public class Session
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

public static final String UID_COLUMN_NAME = "uid";
public static final String SID_COLUMN_NAME = "sid";
public static final String EXPIRATION_DATE_COLUMN_NAME = "expiration_date";
public static final String ACTIVE_FIELD_NAME = "active";

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------


@DatabaseField(id = true,
               columnName = UID_COLUMN_NAME)

public String uid;

@DatabaseField(canBeNull = false,
               columnName = SID_COLUMN_NAME)

public String sid;

@DatabaseField(canBeNull = false,
               columnName = EXPIRATION_DATE_COLUMN_NAME)

public Date expirationDate;

@DatabaseField(canBeNull = false,
               columnName = ACTIVE_FIELD_NAME)

public Boolean active = true;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public Session()
{
}

public Session(String uid, String sid, Date expirationDate)
{
	this.sid = sid;
	this.uid = uid;
	this.expirationDate = expirationDate;
}

}
