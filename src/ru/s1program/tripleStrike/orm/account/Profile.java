package ru.s1program.tripleStrike.orm.account;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable

/**
 * Профиль пользователя.
 */
public class Profile
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

public static final String ID_COLUMN_NAME = "id";
public static final String FIRST_NAME_COLUMN_NAME = "first_name";
public static final String LAST_NAME_COLUMN_NAME = "last_name";
public static final String PATRONYMIC_COLUMN_NAME = "patronymic";
public static final String VERSION_COLUMN_NAME = "version";

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

@DatabaseField(generatedId = true,
               columnName = ID_COLUMN_NAME)

public Integer id;

@DatabaseField(canBeNull = false,
               width = 60,
               columnName = FIRST_NAME_COLUMN_NAME)

public String firstName;

@DatabaseField(canBeNull = false,
               width = 60,
               columnName = LAST_NAME_COLUMN_NAME)

public String lastName;

@DatabaseField(canBeNull = false,
               width = 60,
               columnName = PATRONYMIC_COLUMN_NAME)

public String patronymic;

@DatabaseField(version = true,
               columnName = VERSION_COLUMN_NAME)

public Integer version;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public Profile()
{
}

}
