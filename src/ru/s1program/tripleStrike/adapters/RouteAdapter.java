package ru.s1program.tripleStrike.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import ru.s1program.tripleStrike.R;
import ru.s1program.tripleStrike.orm.route.audit.Audit;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;

/**
 * Адаптер к списку торговых точек.
 */
public class RouteAdapter extends ArrayAdapter<Audit>
{
private List<Audit> audits;

public RouteAdapter(Context context, int textViewResourceId, List<Audit> objects)
{
	super(context, textViewResourceId, objects);

	audits = objects;
}

@Override
public int getCount()
{
	return audits.size();
}

@Override
public Audit getItem(int position)
{
	return audits.get(position);
}

@Override
public View getView(int position, View convertView, ViewGroup parent)
{
	View v = convertView;
	if (v == null)
	{
		LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = vi.inflate(R.layout.route_list_item, null);
	}

	Audit audit = audits.get(position);
	if (audit != null)
	{
		TextView itemIndex = (TextView) v.findViewById(R.id.itemIndex);
		NumberFormat formatter = new DecimalFormat("00");
		itemIndex.setText(formatter.format(position + 1));

		TextView outletName = (TextView) v.findViewById(R.id.outletName);
		outletName.setText(audit.outlet.name);

		TextView outletAddress = (TextView) v.findViewById(R.id.outletAddress);
		outletAddress.setText(audit.outlet.address);

		ImageView warningTriangle = (ImageView) v.findViewById(R.id.warningTriangle);
		warningTriangle.setVisibility(audit.synced ? View.GONE : View.VISIBLE);

		ImageView checkedIcon = (ImageView) v.findViewById(R.id.checkedIcon);
		checkedIcon.setVisibility((audit.synced && audit.startDate != null) ? View.VISIBLE : View.GONE);
	}

	return v;
}
}
