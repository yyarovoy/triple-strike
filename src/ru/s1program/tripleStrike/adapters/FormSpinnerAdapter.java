package ru.s1program.tripleStrike.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class FormSpinnerAdapter extends ArrayAdapter<String>
{

public FormSpinnerAdapter(Context context, int textViewResourceId, List<String> objects)
{
	super(context, textViewResourceId, objects);
}

public View getDropDownView(int position, View convertView, ViewGroup parent)
{
	View v;

	if (position == 0 || position == 2)
	{
		TextView tv = new TextView(getContext());
		tv.setHeight(0);
		tv.setVisibility(View.GONE);
		v = tv;
	}
	else
	{
		// Pass convertView as null to prevent reuse of special case views
		v = super.getDropDownView(position, null, parent);
	}

	// Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
	parent.setVerticalScrollBarEnabled(false);
	return v;
}
}
