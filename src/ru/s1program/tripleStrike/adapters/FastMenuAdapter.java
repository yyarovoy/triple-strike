package ru.s1program.tripleStrike.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ru.s1program.tripleStrike.R;
import ru.s1program.tripleStrike.orm.route.unit.UnitKind;
import ru.s1program.tripleStrike.orm.route.unit.UnitType;
import ru.s1program.tripleStrike.utils.MenuItem;

import java.util.List;

/**
 * Адаптер для меню быстрой навигации
 * на странице с аудитом точки
 */
public class FastMenuAdapter extends ArrayAdapter<MenuItem>
{

private List<MenuItem> items;
private Context _context;

public FastMenuAdapter(Context context, int textViewResourceId, List<MenuItem> objects)
{
	super(context, textViewResourceId);
	_context = context;
	items = objects;
}

public int getCount()
{
	return items.size();
}

public MenuItem getItem(int i)
{
	if (items.get(i) != null)
	{
		return items.get(i);
	}
	else
	{
		return null;
	}
}

@Override
public View getView(int position, View convertView, ViewGroup parent)
{
	View v = convertView;
	if (v == null)
	{
		LayoutInflater vi = (LayoutInflater) _context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		v = vi.inflate(R.layout.fast_menu_list_item, null);
	}
	MenuItem item = items.get(position);
	if (item != null)
	{
		TextView typeName = (TextView) v.findViewById(R.id.fastMenuTypeName);

		if (item.getIsBrand())
		{
			typeName.setText("Бренды");
		}
		else
		{
			UnitKind kind = UnitKind.fromCode(item.getKindId());
			UnitType type = UnitType.fromCode(item.getTypeId());
			String text = type.toString() + ": " + kind.toString();
			typeName.setText(text);
		}

	}
	return v;
}


}
