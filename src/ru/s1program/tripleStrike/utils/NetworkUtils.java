package ru.s1program.tripleStrike.utils;


import android.content.Context;
import android.net.ConnectivityManager;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class NetworkUtils {

    private volatile static NetworkUtils instance;
    private NetworkUtils(){}
    public static NetworkUtils getInstance(){
        if (instance == null){
            synchronized (NetworkUtils.class){
                if (instance == null)
                    instance = new NetworkUtils();
            }
        }
        return instance;
    }


    /**
     * Через рефлексию мы сможем принудительно включить подключение к интернету
     * @param context  Контекст
     * @param enabled  Включить, выключить
     * @throws ClassNotFoundException
     * @throws NoSuchFieldException
     * @throws IllegalAccessException
     * @throws NoSuchMethodException
     * @throws InvocationTargetException
     */
    private void setMobileDataEnabled(Context context, boolean enabled) throws ClassNotFoundException,
            NoSuchFieldException, IllegalAccessException,
            NoSuchMethodException, InvocationTargetException {
        final ConnectivityManager conman = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final Class conmanClass = Class.forName(conman.getClass().getName());
        final Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
        iConnectivityManagerField.setAccessible(true);
        final Object iConnectivityManager = iConnectivityManagerField.get(conman);
        final Class iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
        final Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);
        setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
    }


}
