package ru.s1program.tripleStrike.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.*;
import android.os.Bundle;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

public class GPSUtils {
    private volatile static GPSUtils instance;
    private GPSUtils(){}
    public static GPSUtils getInstance(){
        if (instance == null){
            synchronized (GPSUtils.class){
                if (instance == null)
                    instance = new GPSUtils();
            }
        }
        return instance;
    }

    public void testGPS(Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        locationManager.addTestProvider("Test", false, false, false, false, false, false, false, Criteria.POWER_LOW, Criteria.ACCURACY_FINE);
        locationManager.setTestProviderEnabled("Test", true);

        // Set up your test

        Location location = new Location("Test");
        location.setLatitude(10.0);
        location.setLongitude(20.0);
        locationManager.setTestProviderLocation("Test", location);

        // Check if your listener reacted the right way

        locationManager.removeTestProvider("Test");
    }

    private void showGpsOptions(Context context){
        Intent gpsOptionsIntent = new Intent(
                android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        context.startActivity(gpsOptionsIntent);
    }


    private void createGpsDisabledAlert(final Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Your GPS is disabled! Would you like to enable it?")
                .setCancelable(false)
                .setPositiveButton("Enable GPS",
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int id){
                                showGpsOptions(context);
                            }
                        });
        builder.setNegativeButton("Do nothing",
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int id){
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }


    public GPSPoint getCurrentLocation(Context context)
    {
        String bestProvider;
        List<Address> user = null;
        double lat;
        double lng;

        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            createGpsDisabledAlert(context);
        }
        Criteria criteria = new Criteria();
        bestProvider = lm.getBestProvider(criteria, false);
        Location location = lm.getLastKnownLocation(bestProvider);

        if (location == null){
            lat = 0;
            lng = 0;
        }else{
           Geocoder geoCoder = new Geocoder(context);

            try {
                user = geoCoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            } catch (IOException e) {
                 e.printStackTrace();
            }
            lat=(double)user.get(0).getLatitude();
            lng=(double)user.get(0).getLongitude();



        }

        return new GPSPoint(lat,lng);
    }
}
