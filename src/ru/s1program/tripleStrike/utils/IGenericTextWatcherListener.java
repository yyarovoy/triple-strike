package ru.s1program.tripleStrike.utils;


import android.text.Editable;
import android.view.View;

public interface IGenericTextWatcherListener
{

void afterTextChanged(View view, Editable editable);

}
