package ru.s1program.tripleStrike.utils;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

public class GenericTextWatcher implements TextWatcher
{

private View view;
private IGenericTextWatcherListener listener;

public GenericTextWatcher(View view, IGenericTextWatcherListener listener)
{
	this.view = view;
	this.listener = listener;
}

public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

public void afterTextChanged(Editable editable)
{
	listener.afterTextChanged(view, editable);
}
}