package ru.s1program.tripleStrike.utils;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

// Утилиты общего назначения
public class MainUtils {
    private volatile static MainUtils instance;
    private MainUtils(){}
    public static MainUtils getInstance(){
        if (instance == null){
            synchronized (ImageUtils.class){
                if (instance == null)
                    instance = new MainUtils();
            }
        }
        return instance;
    }

    // конвертация массива байтов SHA1 алгоритма в hex-строку
    private String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        int length = data.length;
        for(int i = 0; i < length; ++i) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            }
            while(++two_halfs < 1);
        }
        return buf.toString();
    }

    // получаем hex-строку, закодированную  SHA-1 алгоритмом
    public String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] sha1hash = new byte[40];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        sha1hash = md.digest();
        return convertToHex(sha1hash);
    }


}
