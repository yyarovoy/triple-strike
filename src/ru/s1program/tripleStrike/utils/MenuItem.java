package ru.s1program.tripleStrike.utils;


public class MenuItem
{

private int _kind_id;
private int _type_id;

private Boolean isBrand = false;

public MenuItem() {}

public MenuItem(int kind_id, int type_id)
{
	_kind_id = kind_id;
	_type_id = type_id;
}

public MenuItem(Boolean isBrand)
{
	this.isBrand = isBrand;
}


public int getKindId()
{
	return _kind_id;
}

public int getTypeId()
{
	return _type_id;
}

public Boolean getIsBrand()
{
	return isBrand;
}

public void setTypeId(int type_id)
{
	_type_id = type_id;
}

public void setKindId(int kind_id)
{
	_kind_id = kind_id;
}

@Override
public boolean equals(Object obj)
{
	if (!(obj instanceof MenuItem))
	{
		return false;
	}
	return this._kind_id == ((MenuItem) obj)._kind_id && this._type_id == ((MenuItem) obj)._type_id;
}

@Override
public int hashCode()
{
	Integer sum = this._kind_id + this._type_id;
	return this._kind_id == 0 && this._type_id == 0 ? 0 : sum.hashCode();
}
}
