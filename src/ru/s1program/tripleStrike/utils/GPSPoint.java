package ru.s1program.tripleStrike.utils;


public class GPSPoint {

    private double _lat;
    private double _lng;

    public GPSPoint(double latitude, double longitude){
        this._lat = latitude;
        this._lng = longitude;
    }

    public double getLatitude()
    {
        return _lat;
    }

    public double getLongitude()
    {
        return _lng;
    }
}
