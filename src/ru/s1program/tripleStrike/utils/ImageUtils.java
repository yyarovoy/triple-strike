package ru.s1program.tripleStrike.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

// Утилиты обработки изображений
public class ImageUtils
{
private static final String IMAGES_PATH = "/triple-strike/images/";
private volatile static ImageUtils instance;

private ImageUtils() {}

public static ImageUtils getInstance()
{
	if (instance == null)
	{
		synchronized (ImageUtils.class)
		{
			if (instance == null)
			{
				instance = new ImageUtils();
			}
		}
	}
	return instance;
}

public String getImageStoragePath()
{
	return Environment.getExternalStorageDirectory().getAbsolutePath() + IMAGES_PATH;
}

// Сохранение изображения с камеры на карту
public String saveCameraImage(Bitmap bmp) throws IOException
{
	String filename = null;
	File path = new File(Environment.getExternalStorageDirectory(), IMAGES_PATH);
	if (!path.exists())
	{
		path.mkdirs();
	}
	// генерируем уникальный id
	UUID uid = UUID.randomUUID();
	try
	{
		// id проводим через SHA1 для верности
		filename = MainUtils.getInstance().SHA1(uid.toString()) + ".jpg";
	}
	catch (NoSuchAlgorithmException e)
	{
		e.printStackTrace();
	}
	catch (UnsupportedEncodingException e)
	{
		e.printStackTrace();
	}
	// сохраняем массив байтов в файл
	File f = new File(path, filename);
	f.createNewFile();
	FileOutputStream out = new FileOutputStream(f);
	bmp.compress(Bitmap.CompressFormat.JPEG, 60, out);
	out.flush();
	out.close();
	return filename;
}

// Получение временного файла изображения,
// сделанного с камеры
public Uri getTempUri(Context context)
{
	//it will return /sdcard/filename.tmp
	final File path = new File(Environment.getExternalStorageDirectory(), context.getPackageName());
	if (!path.exists())
	{
		path.mkdir();
	}

	return Uri.fromFile(new File(path, "image.jpg"));
}
}
