package ru.s1program.tripleStrike.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import com.j256.ormlite.dao.Dao;
import roboguice.inject.InjectView;
import ru.s1program.tripleStrike.R;
import ru.s1program.tripleStrike.TripleStrikeApp;
import ru.s1program.tripleStrike.orm.route.audit.AuditPhoto;
import ru.s1program.tripleStrike.orm.route.unit.UnitPhoto;

import java.io.FileNotFoundException;
import java.sql.SQLException;



public class ViewPhotoActivity extends TripleStrikeActivity {
    @InjectView(R.id.viewPhoto)
    private ImageView photoView;
    private static final String AUDIT_PHOTO = "audit";
    private static final String UNIT_PHOTO = "unit";

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_photo_layout);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        loadPhoto();
    }

    public void loadPhoto(){
        String photo_type = getIntent().getStringExtra("photo_type");
        int photo_id = getIntent().getIntExtra("photo_id",1);
        if (photo_type.equals(AUDIT_PHOTO)){
            try {
                Dao<AuditPhoto, Integer> imageDao = getDbHelper().getAuditPhotoDao();
                AuditPhoto photo = imageDao.queryForId(photo_id);
                Bitmap bmp = photo.getBitmap();
                photoView.setImageBitmap(bmp);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        } else if (photo_type.equals(UNIT_PHOTO))
        {
            try {
                Dao<UnitPhoto, Integer> imageDao = getDbHelper().getUnitPhotoDao();
                UnitPhoto photo = imageDao.queryForId(photo_id);
                Bitmap bmp = photo.getBitmap();
                photoView.setImageBitmap(bmp);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public void backToAuditScreen_onClick(View view)
    {
            onBackPressed();
    }

}
