package ru.s1program.tripleStrike.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import com.google.inject.Inject;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import roboguice.activity.RoboActivity;
import ru.s1program.tripleStrike.TripleStrikeApp;
import ru.s1program.tripleStrike.orm.DbOpenHelper;
import ru.s1program.tripleStrike.orm.account.Account;
import ru.s1program.tripleStrike.orm.account.Profile;
import ru.s1program.tripleStrike.orm.account.Session;
import ru.s1program.tripleStrike.orm.route.Route;
import ru.s1program.tripleStrike.orm.route.audit.Audit;
import ru.s1program.tripleStrike.services.DataUploadService;
import ru.s1program.tripleStrike.vos.ActionVO;

import java.sql.SQLException;
import java.util.Date;

public class TripleStrikeActivity extends RoboActivity
{

protected static final int PROGRESS_DIALOG_ID = 1;
protected static final int WARN_DIALOG_ID = 2;
protected static final int ERROR_DIALOG_ID = 3;

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private DbOpenHelper dbOpenHelper;

// ----------------------------------------------------------------------
// Protected props
// ----------------------------------------------------------------------

@Inject
protected TripleStrikeApp app;

protected String title;
protected String message;
protected View requestFocusView = null;

// ----------------------------------------------------------------------
// Protected methods
// ----------------------------------------------------------------------

@Override
protected void onStop()
{
	super.onStop();

	releaseDbHelper();
}

@Override
protected Dialog onCreateDialog(int id)
{
	switch (id)
	{
		case PROGRESS_DIALOG_ID:
			ProgressDialog progressDialog = new ProgressDialog(this);
			progressDialog.setTitle(title);
			progressDialog.setMessage(message);
			progressDialog.setIndeterminate(true);
			progressDialog.setCancelable(true);
			return progressDialog;

		case ERROR_DIALOG_ID:
			return new AlertDialog.Builder(this).setTitle("Ошибка!")
			                                    .setMessage(message)
			                                    .setCancelable(false)
			                                    .setPositiveButton("OK", new DialogInterface.OnClickListener()
			                                    {
				                                    public void onClick(DialogInterface dialog, int id)
				                                    {
					                                    if (requestFocusView != null)
					                                    {
						                                    requestFocusView.requestFocus();
						                                    requestFocusView = null;
					                                    }

					                                    title = null;
					                                    message = null;
					                                    removeDialog(ERROR_DIALOG_ID);
				                                    }
			                                    })
			                                    .create();

		case WARN_DIALOG_ID:
			return new AlertDialog.Builder(this).setTitle("Внимание")
			                                    .setMessage(message)
			                                    .setCancelable(false)
			                                    .setPositiveButton("OK", new DialogInterface.OnClickListener()
			                                    {
				                                    public void onClick(DialogInterface dialog, int id)
				                                    {
					                                    if (requestFocusView != null)
					                                    {
						                                    requestFocusView.requestFocus();
						                                    requestFocusView = null;
					                                    }

					                                    title = null;
					                                    message = null;
					                                    removeDialog(WARN_DIALOG_ID);
				                                    }
			                                    })
			                                    .create();

	}

	return null;
}

protected void startDataUploadService() throws SQLException
{
	Intent intent = new Intent(this, DataUploadService.class);
	startService(intent);
}

protected void invalidateAudit(Audit audit) throws SQLException
{
	Dao<Audit, Integer> auditDao = getDbHelper().getAuditDao();

	if (audit.startDate == null)
	{
		audit.startDate = new Date();
	}

	audit.synced = false;
	auditDao.update(audit);
}

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

public DbOpenHelper getDbHelper()
{
	if (dbOpenHelper == null)
	{
		dbOpenHelper = OpenHelperManager.getHelper(this, DbOpenHelper.class);
	}
	return dbOpenHelper;
}

public Boolean isLoggedIn()
{
	return (resumeSession() != null);
}

public Session resumeSession()
{
	try
	{
		Dao<Session, String> sessionDao = getDbHelper().getSessionDao();

		if (sessionDao.countOf() > 0)
		{
			PreparedQuery<Session> query = sessionDao.queryBuilder()
			                                         .where()
			                                         .eq(Session.ACTIVE_FIELD_NAME, true)
			                                         .and()
			                                         .gt(Session.EXPIRATION_DATE_COLUMN_NAME, new Date())
			                                         .prepare();
			return sessionDao.queryForFirst(query);
		}
		return null;
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Проблема при взаимодействии с локальной базой данных.", e);
	}

	return null;
}

public Account resumeAccount()
{
	final Session actualSession = resumeSession();

	if (resumeSession() == null)
	{
		return null;
	}

	try
	{
		final Dao<Account, String> accountDao = getDbHelper().getAccountDao();

		PreparedQuery<Account> accountQuery = accountDao.queryBuilder().where().eq(Account.SESSION_COLUMN_NAME,
		                                                                           actualSession).prepare();

		return accountDao.queryForFirst(accountQuery);

	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Проблема при считывании данных аккаунта с устройства.", e);
	}

	return null;
}

public void logout() throws SQLException
{
	Session session = resumeSession();

	if (session == null)
	{
		return;
	}

	Dao<Session, String> sessionDao = getDbHelper().getSessionDao();
	session.active = false;
	sessionDao.update(session);

	Intent intent = new Intent(this, LoginActivity.class);
	startActivity(intent);
	finish();
}

public Profile resumeProfile()
{
	Account actualAccount = resumeAccount();

	if (actualAccount == null)
	{
		return null;
	}

	try
	{
		getDbHelper().getAccountDao().refresh(actualAccount);

		return actualAccount.profile;
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Проблема при считывании данных аккаунта с устройства.", e);
	}

	return null;
}

public Route resumeRoute()
{
	Account actualAccount = resumeAccount();

	if (actualAccount == null)
	{
		return null;
	}

	try
	{
		getDbHelper().getAccountDao().refresh(actualAccount);

		return actualAccount.route;
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Проблема при считывании данных аккаунта с устройства.", e);
	}

	return null;
}

public void resetAllSessions() throws SQLException
{
	final DbOpenHelper dbHelper = getDbHelper();
	final Dao<Session, String> sessionDao = dbHelper.getSessionDao();

	CloseableIterator<Session> sessionIterator = sessionDao.closeableIterator();
	try
	{
		while (sessionIterator.hasNext())
		{
			Session oldSession = sessionIterator.next();
			oldSession.active = false;
			sessionDao.update(oldSession);
		}
	}
	finally
	{
		sessionIterator.close();
	}
}

public void releaseDbHelper()
{
	if (dbOpenHelper != null)
	{
		OpenHelperManager.releaseHelper();
		dbOpenHelper = null;
	}
}

public void asyncTask_onExecuted(ActionVO actionVO)
{
}

}
