package ru.s1program.tripleStrike.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.j256.ormlite.dao.CloseableIterator;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import ru.s1program.tripleStrike.R;
import ru.s1program.tripleStrike.adapters.RouteAdapter;
import ru.s1program.tripleStrike.orm.route.Route;
import ru.s1program.tripleStrike.orm.route.audit.Audit;
import ru.s1program.tripleStrike.services.DataUploadService;
import ru.s1program.tripleStrike.tasks.RouteLoadTask;
import ru.s1program.tripleStrike.vos.ActionVO;
import ru.s1program.tripleStrike.vos.ErrorVO;

import java.sql.SQLException;
import java.util.ArrayList;

@ContentView(R.layout.route)
public class RouteActivity extends TripleStrikeActivity
{

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

@InjectView(R.id.listView)
private ListView listView;

private ArrayList<Audit> audits;

RouteAdapter routeListAdapter;

private BroadcastReceiver broadcastReceiver = new BroadcastReceiver()
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		updateUI(intent);
	}
};

// ----------------------------------------------------------------------
// Private methods
// ----------------------------------------------------------------------

private void updateAuditList(Route route)
{
	CloseableIterator<Audit> iterator = route.audits.closeableIterator();

	try
	{
		audits = new ArrayList<Audit>();

		try
		{
			while (iterator.hasNext())
			{
				audits.add(iterator.next());
			}
		}
		finally
		{
			iterator.close();
		}

		routeListAdapter = new RouteAdapter(this, R.layout.route_list_item, audits);
		listView.setAdapter(routeListAdapter);
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Ошибка чтения базы данных.", e);

		message = "Ошибка чтения данных с устройства.";
		showDialog(ERROR_DIALOG_ID);
	}
}

// ----------------------------------------------------------------------
// Protected methods
// ----------------------------------------------------------------------

@Override
protected void onCreate(Bundle savedInstanceState)
{
	super.onCreate(savedInstanceState);

	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	try
	{
		startDataUploadService();
	}
	catch (SQLException e)
	{
		e.printStackTrace();
	}

	RelativeLayout listViewHeader = (RelativeLayout) getLayoutInflater().inflate(R.layout.red_header, null);
	TextView headerTextView = (TextView) listViewHeader.findViewById(R.id.headerTextView);
	headerTextView.setText(getText(R.string.route_screen_title));

	listView.addHeaderView(listViewHeader, null, false);

	AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener()
	{
		public void onItemClick(AdapterView<?> parent, View v, int position, long id)
		{
			Audit audit = audits.get(position - listView.getHeaderViewsCount());
			Intent intent = new Intent(getApplicationContext(), AuditActivity.class);
			intent.putExtra("audit_id", audit.id);
			intent.putExtra("prev_activity", "route");
			startActivity(intent);
		}
	};

	listView.setOnItemClickListener(itemClickListener);

	if (!isLoggedIn())
	{
		return;
	}

	Route route = resumeRoute();
	if (route != null)
	{
		updateAuditList(route);
	}
	else
	{
		title = "Синхронизация";
		message = "Получение данных маршрута…";
		showDialog(PROGRESS_DIALOG_ID);

		new RouteLoadTask(this).execute();
	}
}

@Override
protected void onResume()
{
	super.onResume();

	if (!isLoggedIn())
	{
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
		finish();
		return;
	}

	registerReceiver(broadcastReceiver, new IntentFilter(DataUploadService.DATA_SYNCED_EVENT));
}

@Override
protected void onPause()
{
	super.onPause();

	unregisterReceiver(broadcastReceiver);
}

private void updateUI(Intent intent)
{
	Route route = resumeRoute();
	if (route != null)

        Log.d(getClass().getName(), "update ui");
	{
/*
		int visiblePosition = listView.getFirstVisiblePosition();
		updateAuditList(route);
		listView.smoothScrollToPosition(visiblePosition);
//		listView.setSelection(visiblePosition);
*/
        int index = listView.getFirstVisiblePosition();
        View v = listView.getChildAt(0);
        int top = (v == null) ? 0 : v.getTop();
        listView.setSelectionFromTop(index, top);
	}
}

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

@Override
public void asyncTask_onExecuted(ActionVO actionVO)
{
	super.asyncTask_onExecuted(actionVO);

	title = null;
	message = null;
	removeDialog(PROGRESS_DIALOG_ID);

	if (actionVO.getStatus() == 0 || actionVO.getResult() == 0)
	{
		if (actionVO.getErrors() != null && actionVO.getErrors().size() > 0)
		{
			message = "";
			for (ErrorVO errorVO : actionVO.getErrors())
			{
				message += errorVO.message;
			}
		}
		else
		{
			message = "Ошибка обработки запроса сервером.";
		}

		showDialog(ERROR_DIALOG_ID);
	}
	else if (resumeRoute() == null)
	{
		message = "Ошибка чтения данных о маршруте с устройства.";
		showDialog(ERROR_DIALOG_ID);
	}
	else
	{
		updateAuditList(resumeRoute());
	}
}

public void logoutButton_onClick(View view)
{
	try
	{
		logout();
	}
	catch (SQLException e)
	{
		message = "Ошибка записи данных на устройство.";
		showDialog(ERROR_DIALOG_ID);

		Log.e(getClass().getName(), message, e);
	}
}
}
