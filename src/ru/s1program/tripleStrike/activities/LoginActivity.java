package ru.s1program.tripleStrike.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import ru.s1program.tripleStrike.R;
import ru.s1program.tripleStrike.tasks.LoginTask;
import ru.s1program.tripleStrike.vos.ActionVO;
import ru.s1program.tripleStrike.vos.ErrorVO;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@ContentView(R.layout.login)
public class LoginActivity extends TripleStrikeActivity
{

private static final Pattern PHONE_NUMBER_CODE_PATTERN = Pattern.compile("^(89|\\+79)");
private static final Pattern PHONE_NUMBER_PATTERN = Pattern.compile("^(89|\\+79)[\\d\\- ]{9}$");

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

@InjectView(R.id.loginTextInput)
private EditText loginTextInput;

@InjectView(R.id.passwordTextInput)
private EditText passwordTextInput;

@InjectView(R.id.phoneTextInput)
private EditText phoneTextInput;

// ----------------------------------------------------------------------
// Private methods
// ----------------------------------------------------------------------

private void doLogin(String login, String password, String phone)
{
	title = "Авторизация";
	message = "Производится авторизация на сервере…";
	showDialog(PROGRESS_DIALOG_ID);

	new LoginTask(this).execute(login, password, phone);
}

// ----------------------------------------------------------------------
// Protected methods
// ----------------------------------------------------------------------

@Override
protected void onCreate(Bundle savedInstanceState)
{
	super.onCreate(savedInstanceState);

	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	try
	{
		startDataUploadService();
	}
	catch (SQLException e)
	{
		e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
	}
}

@Override
protected void onResume()
{
	super.onResume();

	if (isLoggedIn())
	{
		Intent intent;

		if (resumeProfile() != null)
		{
			intent = new Intent(this, RouteActivity.class);
		}
		else
		{
			intent = new Intent(this, ProfileActivity.class);
		}

		startActivity(intent);
	}
}

// ----------------------------------------------------------------------
// Event handlers
// ----------------------------------------------------------------------

public void loginButton_onClick(View view)
{
	final String login = loginTextInput.getText().toString();
	final String password = passwordTextInput.getText().toString();
	final String phone = phoneTextInput.getText().toString();

	if (login.length() <= 0)
	{
		message = "Введите логин.";
		requestFocusView = loginTextInput;
		showDialog(ERROR_DIALOG_ID);
		return;
	}
	else if (password.length() <= 0)
	{
		message = "Введите пароль.";
		requestFocusView = passwordTextInput;
		showDialog(ERROR_DIALOG_ID);
		return;
	}
	else if (phone.length() <= 0)
	{
		message = "Укажите номер телефона.";
		requestFocusView = phoneTextInput;
		showDialog(ERROR_DIALOG_ID);
		return;
	}

	final Matcher phoneMatcher = PHONE_NUMBER_PATTERN.matcher(phone);
	if (!phoneMatcher.matches())
	{
		message = "Неверно указан номер телефона.\nТелефон должен быть вида +79XXXXXXXXX или 89XXXXXXXXX.";
		requestFocusView = phoneTextInput;
		showDialog(ERROR_DIALOG_ID);
		return;
	}

	Matcher matcher = PHONE_NUMBER_CODE_PATTERN.matcher(phone);
	String formattedPhone = matcher.replaceFirst("79");

	doLogin(login, password, formattedPhone);
}

@Override
public void asyncTask_onExecuted(ActionVO actionVO)
{
	super.asyncTask_onExecuted(actionVO);

	title = null;
	message = null;
	removeDialog(PROGRESS_DIALOG_ID);

	if (actionVO.getStatus() == 0 || actionVO.getResult() == 0)
	{
		if (actionVO.getErrors() != null && actionVO.getErrors().size() > 0)
		{
			message = "";
			for (ErrorVO errorVO : actionVO.getErrors())
			{
				message += errorVO.message;
			}
		}
		else
		{
			message = "Ошибка обработки запроса сервером.";
		}

		showDialog(ERROR_DIALOG_ID);
	}
	else
	{
		Intent intent = new Intent(this, ProfileActivity.class);
		startActivity(intent);
	}
}
}