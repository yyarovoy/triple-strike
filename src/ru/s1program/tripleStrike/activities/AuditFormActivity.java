package ru.s1program.tripleStrike.activities;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import ru.s1program.tripleStrike.R;
import ru.s1program.tripleStrike.adapters.FormSpinnerAdapter;
import ru.s1program.tripleStrike.orm.route.audit.Audit;
import ru.s1program.tripleStrike.orm.route.brand.Brand;
import ru.s1program.tripleStrike.orm.route.brand.BrandResult;
import ru.s1program.tripleStrike.orm.route.unit.*;
import ru.s1program.tripleStrike.utils.GenericTextWatcher;
import ru.s1program.tripleStrike.utils.IGenericTextWatcherListener;
import ru.s1program.tripleStrike.utils.ImageUtils;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@ContentView(R.layout.audit_form)

public class AuditFormActivity extends TripleStrikeActivity implements IGenericTextWatcherListener
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

/**
 * Состояние, передающееся в активити камеры
 */
private static final int CAMERA_REQUEST = 1;
private static final int ASSORTMENT_COLUMNS_NUM = 6;
private static final String ASSORTMENT_NAME_LABEL = "Товар";
private static final String ASSORTMENT_VALUE_LABEL = "Наличие";
private static final int EQUIPMENT_COLUMNS_NUM = 2;
private static final String EQUIPMENT_NAME_LABEL = "Оборудование";
private static final String EQUIPMENT_VALUE_LABEL = "Наличие";
private static final int ADDITIONAL_EQUIPMENT_COLUMNS_NUM = 2;
private static final String ADDITIONAL_EQUIPMENT_NAME_LABEL = "Оборудование";
private static final String ADDITIONAL_EQUIPMENT_VALUE_LABEL = "Наличие";
private static final int BRAND_COLUMNS_NUM = 5;

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

@InjectView(R.id.outletName)
private TextView outletName;

@InjectView(R.id.outletAddress)
private TextView outletAddress;

@InjectView(R.id.assortmentTitle)
private TextView tableTitle;

@InjectView(R.id.tableLayout)
private TableLayout tableLayout;

@InjectView(R.id.prevButton)
private Button prevButton;

@InjectView(R.id.nextButton)
private Button nextButton;

@InjectView(R.id.finishButton)
private Button finishButton;

private Audit audit;
private Boolean isBrand;
private Boolean wizardMode;
private UnitKind kind;
private UnitType type;

private List<String> menuItems;
private LinearLayout headerPhotos;
private TextView photosCount;
private Dao<UnitPhoto, Integer> photoDao;
private int makePhotosCount;
public static final int MAX_PHOTOS_COUNT = 8;

// ----------------------------------------------------------------------
// Private methods
// ----------------------------------------------------------------------

/**
 * Загрузка фоток для аудита
 */
public void loadPhotos()
{

	photosCount = (TextView) findViewById(R.id.photoCountLabel);
	headerPhotos = (LinearLayout) findViewById(R.id.photosLayout);
	headerPhotos.removeAllViews();
	// список фоток
	List<UnitPhoto> photoList;
	try
	{
		photoDao = getDbHelper().getUnitPhotoDao();
		photoList = photoDao.queryBuilder()
		                    .where()
		                    .eq(UnitPhoto.KIND_COLUMN_NAME, kind)
		                    .and()
		                    .eq(UnitPhoto.TYPE_COLUMN_NAME, type)
		                    .query();
		if (photoList.size() > 0)
		{
			int i = 0;
			// проходимся по каждой найденной фотке
			for (UnitPhoto photo : photoList)
			{
				i++;
				TextView photoButton = new TextView(this);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(90, 35);
				photoButton.setTextAppearance(this, R.style.HyperLink);
				SpannableString content = new SpannableString("Фото-" + i);
				content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
				photoButton.setText(content);
				params.setMargins(0, 10, 0, 0);
				photoButton.setTag(photo);
				photoButton.setLayoutParams(params);
				photoButton.setOnClickListener(viewPhotoListener);
				headerPhotos.addView(photoButton);
			}
			makePhotosCount = photoList.size();
		}
		else
		{
			makePhotosCount = 0;
		}
		String photosCountText = "Вы загрузили " + makePhotosCount + " фото:";
		photosCount.setText(photosCountText);
	}
	catch (SQLException e)
	{
		e.printStackTrace();
	}
}

private void initFields() throws SQLException
{
	final Intent intent = getIntent();

	int auditId = intent.getIntExtra("audit_id", 1);
	int typeId = intent.getIntExtra("type_id", 2);
	int kindId = intent.getIntExtra("kind_id", 1);

	isBrand = intent.getBooleanExtra("is_brand", false);
	wizardMode = intent.getBooleanExtra("wizard_mode", false);
	type = UnitType.fromCode(typeId);
	kind = UnitKind.fromCode(kindId);

	Dao<Audit, Integer> auditDao = getDbHelper().getAuditDao();
	audit = auditDao.queryForId(auditId);
	auditDao.refresh(audit);

	menuItems = Arrays.asList(getResources().getStringArray(R.array.spinner_menu));

	outletName.setText(audit.outlet.name);
	outletAddress.setText(audit.outlet.address);

	prevButton.setVisibility((wizardMode && hasPrev()) ? View.VISIBLE : View.GONE);
	nextButton.setVisibility((wizardMode && hasNext()) ? View.VISIBLE : View.GONE);
	finishButton.setVisibility((!wizardMode || hasNext()) ? View.GONE : View.VISIBLE);
}

private void generateTable() throws SQLException
{
	if (isBrand)
	{
		generateBrandTable();
	}
	else if (type == UnitType.EQUIPMENT)
	{
		generateEquipmentTable();
	}
	else if (type == UnitType.ADDITIONAL_EQUIPMENT)
	{
		generateAdditionalEquipmentTable();
	}
	else
	{
		generateAssortmentTable();
	}
}

private void generateAssortmentTable() throws SQLException
{
	tableTitle.setText(type + ": " + kind);

	tableLayout.setStretchAllColumns(true);

	ArrayList<UnitResult> unitResults = getUnitResults(audit, type, kind);

	generateUnitTable(unitResults, ASSORTMENT_COLUMNS_NUM, ASSORTMENT_NAME_LABEL, ASSORTMENT_VALUE_LABEL);
}

private void generateEquipmentTable() throws SQLException
{
	tableTitle.setText(type + ": " + kind);

	tableLayout.setStretchAllColumns(true);

	ArrayList<UnitResult> unitResults = getUnitResults(audit, type, kind);

	generateUnitTable(unitResults, EQUIPMENT_COLUMNS_NUM, EQUIPMENT_NAME_LABEL, EQUIPMENT_VALUE_LABEL);
}

private void generateAdditionalEquipmentTable() throws SQLException
{
	tableTitle.setText(type + ": " + kind);

	tableLayout.setStretchAllColumns(true);

	ArrayList<UnitResult> unitResults = getUnitResults(audit, type, kind);

	generateUnitTable(unitResults,
	                  ADDITIONAL_EQUIPMENT_COLUMNS_NUM,
	                  ADDITIONAL_EQUIPMENT_NAME_LABEL,
	                  ADDITIONAL_EQUIPMENT_VALUE_LABEL);
}

private void generateUnitTable(ArrayList<UnitResult> unitResults, int columnsNum, String nameLabel, String valueLabel)
{
	tableLayout.removeAllViews();

	for (int i = 0; i < unitResults.size(); i = i + columnsNum)
	{
		TableRow row1 = new TableRow(this);
		TableRow row2 = new TableRow(this);

		TextView yellowTextView = (TextView) getLayoutInflater().inflate(R.layout.table_cell_yellow_text, row1, false);
		yellowTextView.setText(nameLabel);
		row1.addView(yellowTextView);

		yellowTextView = (TextView) getLayoutInflater().inflate(R.layout.table_cell_yellow_text, row2, false);
		yellowTextView.setText(valueLabel);
		row2.addView(yellowTextView);


		for (int j = i; j < (i + columnsNum); j++)
		{
			if (j < unitResults.size())
			{
				UnitResult unitResult = unitResults.get(j);

				String mDrawableName = "unit_" + unitResult.unit.id;
				int resID = getResources().getIdentifier(mDrawableName, "drawable", getPackageName());


				LinearLayout labeledIcon = (LinearLayout) getLayoutInflater().inflate(R.layout.cell_icon_labeled,
				                                                                      row1,
				                                                                      false);
				ImageView imageView = (ImageView) labeledIcon.findViewById(R.id.imageView);
				imageView.setImageResource(resID);
				TextView textView = (TextView) labeledIcon.findViewById(R.id.textView);
				textView.setText(unitResult.unit.name);
				row1.addView(labeledIcon);

				if (unitResult.unit.type == UnitType.ASSORTMENT)
				{
					LinearLayout checkBoxCell = (LinearLayout) getLayoutInflater().inflate(R.layout.cell_checkbox,
					                                                                       row2,
					                                                                       false);
					CheckBox checkBox = (CheckBox) checkBoxCell.findViewById(R.id.checkBox);
					checkBox.setTag(unitResult.id);
					checkBox.setChecked(unitResult.result != null && (unitResult.result > 0));
					checkBox.setOnCheckedChangeListener(checkBox_onCheckedChanged);
					row2.addView(checkBoxCell);
				}
				else
				{
					LinearLayout spinnerCell = (LinearLayout) getLayoutInflater().inflate(R.layout.cell_spinner,
					                                                                      row2,
					                                                                      false);

					Spinner spinner = (Spinner) spinnerCell.findViewById(R.id.spinner);
					FormSpinnerAdapter adapter = new FormSpinnerAdapter(this,
					                                                    android.R.layout.simple_spinner_item,
					                                                    menuItems);
					adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinner.setAdapter(adapter);
					spinner.setTag(unitResult.id);
					spinner.setSelection((unitResult.result != null) ? unitResult.result + 1 : 0);
					spinner.setOnItemSelectedListener(spinner_onItemSelected);
					row2.addView(spinnerCell);
				}

			}
			else
			{
				View emptyCell = getLayoutInflater().inflate(R.layout.cell_empty_yellow_light, row1, false);
				row1.addView(emptyCell);

				emptyCell = getLayoutInflater().inflate(R.layout.cell_empty_yellow_light, row2, false);
				row2.addView(emptyCell);
			}

		}

		tableLayout.addView(row1);
		tableLayout.addView(row2);
	}
}

private void generateBrandTable() throws SQLException
{
	tableTitle.setText("Общее количество SKU соответствующих брендов");
	tableLayout.setStretchAllColumns(true);

	Dao<BrandResult, Integer> brandResultDao = getDbHelper().getBandResultDao();

	ArrayList<BrandResult> brandResults = new ArrayList<BrandResult>();

	CloseableIterator<BrandResult> iterator = audit.brandResults.closeableIterator();
	try
	{
		while (iterator.hasNext())
		{
			BrandResult brandResult = iterator.next();
			brandResultDao.refresh(brandResult);
			if (brandResult.brand.id != 6)
			{
				brandResults.add(brandResult);
			}
		}
	}
	finally
	{
		iterator.close();
	}

	for (int i = 0; i < brandResults.size(); i = i + BRAND_COLUMNS_NUM)
	{
		TableRow row1 = new TableRow(this);
		TableRow row2 = new TableRow(this);

		for (int j = i; j < (i + BRAND_COLUMNS_NUM); j++)
		{
			if (j < brandResults.size())
			{
				BrandResult brandResult = brandResults.get(j);
				Integer result = brandResult.result;
				Brand brand = brandResult.brand;

				String mDrawableName = "brand_" + brand.id;
				int resID = getResources().getIdentifier(mDrawableName, "drawable", getPackageName());


				LinearLayout labeledIcon = (LinearLayout) getLayoutInflater().inflate(R.layout.cell_icon_labeled,
				                                                                      row1,
				                                                                      false);
				ImageView imageView = (ImageView) labeledIcon.findViewById(R.id.imageView);
				imageView.setImageResource(resID);
				TextView textView = (TextView) labeledIcon.findViewById(R.id.textView);
				textView.setText(brand.name);
				row1.addView(labeledIcon);

				LinearLayout editTextCell = (LinearLayout) getLayoutInflater().inflate(R.layout.cell_number_input,
				                                                                       row2,
				                                                                       false);
				EditText editText = (EditText) editTextCell.findViewById(R.id.editText);
				editText.setTag(brandResult.id);

				editText.setText(result != null ? "" + result : "");
				editText.addTextChangedListener(new GenericTextWatcher(editText, this));
				row2.addView(editTextCell);

			}
			else
			{
				View emptyCell = getLayoutInflater().inflate(R.layout.cell_empty_yellow_light, row1, false);
				row1.addView(emptyCell);

				emptyCell = getLayoutInflater().inflate(R.layout.cell_empty_yellow_light, row2, false);
				row2.addView(emptyCell);
			}
		}

		tableLayout.addView(row1);
		tableLayout.addView(row2);
	}
}

private ArrayList<UnitResult> getUnitResults(Audit audit, UnitType type, UnitKind kind) throws SQLException
{
	Dao<UnitResult, Integer> unitResultDao = getDbHelper().getUnitResultDao();

	ArrayList<UnitResult> results = new ArrayList<UnitResult>();

	CloseableIterator<UnitResult> iterator = audit.unitResults.closeableIterator();
	try
	{
		while (iterator.hasNext())
		{
			UnitResult unitResult = iterator.next();
			final Unit unit = unitResult.unit;
			if (unit.type == type && unit.kind == kind)
			{
				unitResultDao.refresh(unitResult);
				results.add(unitResult);
			}
		}
	}
	finally
	{
		iterator.close();
	}

	return results;
}

private void updateUnitResult(Integer unitResultId, Integer newValue)
{
	Log.d(getClass().getName(), "updateUnitResult: " + unitResultId + ", " + newValue);

	try
	{
		Dao<UnitResult, Integer> unitResultDao = getDbHelper().getUnitResultDao();

		UnitResult unitResult = unitResultDao.queryForId(unitResultId);
		unitResult.result = newValue;
		unitResultDao.update(unitResult);

		invalidateAudit(audit);
	}
	catch (SQLException e)
	{
		message = "Ошибка сохранения данных на устройство.";
		showDialog(ERROR_DIALOG_ID);

		Log.e(getClass().getName(), message, e);
	}
}

private void updateBrandResult(Integer brandResultId, Integer newValue)
{
	Log.d(getClass().getName(), "updateBrandResult: " + brandResultId + ", " + newValue);

	try
	{
		Dao<BrandResult, Integer> bandResultDao = getDbHelper().getBandResultDao();

		BrandResult brandResult = bandResultDao.queryForId(brandResultId);
		brandResult.result = newValue;
		bandResultDao.update(brandResult);

		invalidateAudit(audit);
	}
	catch (SQLException e)
	{
		message = "Ошибка сохранения данных на устройство.";
		showDialog(ERROR_DIALOG_ID);

		Log.e(getClass().getName(), message, e);
	}
}

private void goToUnit(UnitType type, UnitKind kind)
{
	Intent intent = new Intent(AuditFormActivity.this, AuditFormActivity.class);
	intent.putExtra("audit_id", audit.id);
	intent.putExtra("type_id", type.getCode());
	intent.putExtra("kind_id", kind.getCode());
	intent.putExtra("wizard_mode", true);
	startActivity(intent);
}

private void goToBrands()
{
	Intent intent = new Intent(AuditFormActivity.this, AuditFormActivity.class);
	intent.putExtra("audit_id", audit.id);
	intent.putExtra("is_brand", true);
	intent.putExtra("wizard_mode", true);
	startActivity(intent);
}

private Boolean hasNext()
{
	return !isBrand;
}

private Boolean hasPrev()
{
	return (type != UnitType.ASSORTMENT || kind != UnitKind.JUICES);
}

private void doNext()
{
	if (!hasNext())
	{
		return;
	}

	if (type == UnitType.ASSORTMENT)
	{
		switch (kind)
		{
			case JUICES:
				goToUnit(UnitType.ASSORTMENT, UnitKind.SNACKS);
				break;
			case SNACKS:
				goToUnit(UnitType.ASSORTMENT, UnitKind.DRINKS);
				break;
			case DRINKS:
				goToUnit(UnitType.ASSORTMENT, UnitKind.NPD);
				break;
			case NPD:
				goToUnit(UnitType.EQUIPMENT, UnitKind.JUICES);
				break;
		}
	}
	else if (type == UnitType.EQUIPMENT)
	{
		switch (kind)
		{
			case JUICES:
				goToUnit(UnitType.EQUIPMENT, UnitKind.SNACKS);
				break;
			case SNACKS:
				goToUnit(UnitType.EQUIPMENT, UnitKind.DRINKS);
				break;
			case DRINKS:
				goToUnit(UnitType.ADDITIONAL_EQUIPMENT, UnitKind.JUICES);
				break;
		}
	}
	else if (type == UnitType.ADDITIONAL_EQUIPMENT)
	{
		switch (kind)
		{
			case JUICES:
				goToUnit(UnitType.ADDITIONAL_EQUIPMENT, UnitKind.SNACKS);
				break;
			case SNACKS:
				goToUnit(UnitType.ADDITIONAL_EQUIPMENT, UnitKind.DRINKS);
				break;
			case DRINKS:
				goToBrands();
				break;
		}
	}
}

private void doPrev()
{
	if (!hasPrev())
	{
		return;
	}

	if (isBrand)
	{
		goToUnit(UnitType.ADDITIONAL_EQUIPMENT, UnitKind.DRINKS);
		return;
	}

	if (type == UnitType.ADDITIONAL_EQUIPMENT)
	{
		switch (kind)
		{
			case DRINKS:
				goToUnit(UnitType.ADDITIONAL_EQUIPMENT, UnitKind.SNACKS);
				break;
			case SNACKS:
				goToUnit(UnitType.ADDITIONAL_EQUIPMENT, UnitKind.JUICES);
				break;
			case JUICES:
				goToUnit(UnitType.EQUIPMENT, UnitKind.DRINKS);
				break;
		}
	}
	else if (type == UnitType.EQUIPMENT)
	{
		switch (kind)
		{
			case DRINKS:
				goToUnit(UnitType.EQUIPMENT, UnitKind.SNACKS);
				break;
			case SNACKS:
				goToUnit(UnitType.EQUIPMENT, UnitKind.JUICES);
				break;
			case JUICES:
				goToUnit(UnitType.ASSORTMENT, UnitKind.NPD);
				break;
		}
	}
	else if (type == UnitType.ASSORTMENT)
	{
		switch (kind)
		{
			case NPD:
				goToUnit(UnitType.ASSORTMENT, UnitKind.DRINKS);
				break;
			case DRINKS:
				goToUnit(UnitType.ASSORTMENT, UnitKind.SNACKS);
				break;
			case SNACKS:
				goToUnit(UnitType.ASSORTMENT, UnitKind.JUICES);
				break;
		}
	}
}

// ----------------------------------------------------------------------
// Protected methods
// ----------------------------------------------------------------------

public void onCreate(Bundle savedInstanceState)
{
	super.onCreate(savedInstanceState);
	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        /*try {
            startDataUploadService();
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }*/
}

@Override
protected void onResume()
{
	super.onResume();

	try
	{
		initFields();
		generateTable();
		loadPhotos();
	}
	catch (SQLException e)
	{
		message = "Ошибка чтения данных о торговой точке с устройства.";
		showDialog(ERROR_DIALOG_ID);

		Log.e(getClass().getName(), message, e);
	}
}

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data)
{
	if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK)
	{
		// получаем временный файл изображения
		try
		{
			// создаем битмап из файла и сохраняем его на карту в папку приложения
			Bitmap captureBmp = MediaStore.Images.Media.getBitmap(getContentResolver(),
			                                                      ImageUtils.getInstance()
			                                                                .getTempUri(AuditFormActivity.this));
			String filename = ImageUtils.getInstance().saveCameraImage(captureBmp);
			photoDao = getDbHelper().getUnitPhotoDao();
			String systemPath = ImageUtils.getInstance().getImageStoragePath() + filename;
			UnitPhoto img = new UnitPhoto(filename, systemPath, audit, kind, type, false);
			photoDao.create(img);
			invalidateAudit(audit);
			Toast.makeText(this, "Фотография сохранена", Toast.LENGTH_SHORT).show();
			makePhotosCount += 1;
			//				String photosCountText = "Вы загрузили " + makePhotosCount + " фото:";
			//				photosCount.setText(photosCountText);
			//
			//				TextView photoButton = new TextView(this);
			//				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(90, 35);
			//				photoButton.setTextAppearance(this, R.style.HyperLink);
			//				SpannableString content = new SpannableString("Фото-" + makePhotosCount);
			//				content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
			//				photoButton.setText(content);
			//				params.setMargins(0, 10, 0, 0);
			//				photoButton.setTag(img);
			//				photoButton.setLayoutParams(params);
			//				photoButton.setOnClickListener(viewPhotoListener);
			//				headerPhotos.addView(photoButton);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}

protected View.OnClickListener viewPhotoListener = new View.OnClickListener()
{
	public void onClick(View view)
	{
		TextView sender = (TextView) view;
		Intent viewPhotoIntent = new Intent(AuditFormActivity.this, ViewPhotoActivity.class);
		UnitPhoto photo = (UnitPhoto) sender.getTag();
		viewPhotoIntent.putExtra("photo_id", photo.id);
		viewPhotoIntent.putExtra("photo_type", "unit");
		startActivity(viewPhotoIntent);
	}
};

public void backToAuditScreen_onClick(View view)
{
	if (audit.synced && (getIntent().hasExtra("prev_activity") && getIntent().getStringExtra("prev_activity")
			                                                              .equals("audit")))
	{
		onBackPressed();
	}
	else
	{
		Intent intent = new Intent(this, AuditActivity.class);
		intent.putExtra("audit_id", audit.id);
		startActivity(intent);

		finish();
	}
}

public void takePhotoButton_onClick(View view)
{
	// создаем интент для производства фотографии с камеры
	Intent makeImageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
	makeImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, ImageUtils.getInstance().getTempUri(AuditFormActivity.this));
	// стартуем интент с возвращаемым результатом
	startActivityForResult(makeImageIntent, CAMERA_REQUEST);
}

public void nextButton_onClick(View view)
{
	doNext();
}

public void prevButton_onClick(View view)
{
	doPrev();
}

public void finishButton_onClick(View view)
{
	Intent routeIntent = new Intent(this, RouteActivity.class);
	startActivity(routeIntent);
	finish();
}

protected CompoundButton.OnCheckedChangeListener checkBox_onCheckedChanged = new CompoundButton.OnCheckedChangeListener()
{
	public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked)
	{
		int unitResultId = (Integer) compoundButton.getTag();
		updateUnitResult(unitResultId, isChecked ? 1 : 0);
	}
};

protected Spinner.OnItemSelectedListener spinner_onItemSelected = new Spinner.OnItemSelectedListener()
{
	public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id)
	{
		int unitResultId = (Integer) adapterView.getTag();
		updateUnitResult(unitResultId, (position == 0) ? null : position - 1);
	}

	public void onNothingSelected(AdapterView<?> adapterView)
	{
	}
};

public void afterTextChanged(View view, Editable editable)
{
	final String s = editable.toString();

	if (s == null || s.length() < 1)
	{
		return;
	}

	try
	{
		Integer newValue = Integer.parseInt(s);

		Integer brandResultId = (Integer) view.getTag();
		updateBrandResult(brandResultId, newValue);
	}
	catch (NumberFormatException e)
	{
		message = "Недопустимый символ";
		showDialog(ERROR_DIALOG_ID);

		Log.e(getClass().getName(), message, e);
	}
}

}
