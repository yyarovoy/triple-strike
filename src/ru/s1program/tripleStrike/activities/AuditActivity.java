package ru.s1program.tripleStrike.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import ru.s1program.tripleStrike.R;
import ru.s1program.tripleStrike.adapters.FastMenuAdapter;
import ru.s1program.tripleStrike.orm.route.audit.Audit;
import ru.s1program.tripleStrike.orm.route.audit.AuditPhoto;
import ru.s1program.tripleStrike.orm.route.audit.MonthPoints;
import ru.s1program.tripleStrike.orm.route.unit.Unit;
import ru.s1program.tripleStrike.orm.route.unit.UnitResult;
import ru.s1program.tripleStrike.services.DataUploadService;
import ru.s1program.tripleStrike.utils.GPSPoint;
import ru.s1program.tripleStrike.utils.GPSUtils;
import ru.s1program.tripleStrike.utils.ImageUtils;
import ru.s1program.tripleStrike.utils.MenuItem;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.*;

@ContentView(R.layout.audit)

/**
 * Активити аудита точки
 * Здесь мы загружаем данные по полученным баллам в точке,
 * меню быстрой навигации
 */
public class AuditActivity extends TripleStrikeActivity
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

/**
 * Состояние, передающееся в активити камеры
 */
private static final int CAMERA_REQUEST = 1;

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

@InjectView(R.id.listView)
private ListView listView;
private TextView outletName;
private TextView outletAddress;
private Button takePhotoButton;
private Button startAuditButton;
private CheckBox outletNotExistCheckBox;
private CheckBox prizeDeliveredCheckBox;
private TextView photosCount;
private TextView dataSyncedLabel;
private TextView dataNotSyncedWarning;
private LinearLayout headerPhotos;
private TableLayout pointsTable;
private LinearLayout pointsTableWrapper;
private LinearLayout bottomRedHeaderWrapper;

private Audit audit;

private Dao<AuditPhoto, Integer> imageDao;

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

/**
 * Список пунктов меню.
 */
private ArrayList<MenuItem> items = new ArrayList<MenuItem>();
private FastMenuAdapter listAdapter;

private Boolean outletNotExists = false;
private Boolean needRunWizard = true;
private int makePhotosCount;

public static final int MAX_PHOTOS_COUNT = 8;

private BroadcastReceiver broadcastReceiver = new BroadcastReceiver()
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		try
		{
			getDbHelper().getAuditDao().refresh(audit);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}

		updatePointsTable();
		updateLayout();
	}
};

// ----------------------------------------------------------------------
// Getters and setters
// ----------------------------------------------------------------------

public void setOutletNotExists(Boolean outletNotExists)
{
	this.outletNotExists = outletNotExists;

	try
	{
		Dao<Audit, Integer> auditDao = getDbHelper().getAuditDao();

		audit.notExist = outletNotExists;
		auditDao.update(audit);

		invalidateAudit(audit);
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Ошибка записи в базу данных.", e);

		message = "Ошибка записи данных на устройство.";
		showDialog(ERROR_DIALOG_ID);
	}

	updateLayout();
}

private void setPrizeDelivered(Boolean value)
{
	try
	{
		Dao<Audit, Integer> auditDao = getDbHelper().getAuditDao();

		audit.prize = value;
		auditDao.update(audit);

		invalidateAudit(audit);
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Ошибка записи в базу данных.", e);

		message = "Ошибка записи данных на устройство.";
		showDialog(ERROR_DIALOG_ID);
	}

	updateLayout();
}

// ----------------------------------------------------------------------
// Private methods
// ----------------------------------------------------------------------

/**
 * Загрузка данных по аудиту
 */
private void loadAuditInformation()
{

	CloseableIterator<UnitResult> iterator;
	audit = null;
	try
	{
		Dao<Audit, Integer> auditDao = getDbHelper().getAuditDao();

		Integer auditId = getIntent().getIntExtra("audit_id", 5);
		audit = auditDao.queryForId(auditId);

		needRunWizard = (audit.startDate == null);
		outletNotExists = audit.notExist;

		iterator = audit.unitResults.closeableIterator();
		try
		{
			while (iterator.hasNext())
			{
				UnitResult unitResult = iterator.next();
				final Unit unit = unitResult.unit;
				// вносим для каждого элемента uniResult элемент в список пунктов меню
				MenuItem item = new MenuItem(unit.kind.getCode(), unit.type.getCode());
				items.add(item);
			}
		}
		finally
		{
			iterator.close();
		}

		// здесь заносим наш массив в связный список
		// для того чтобы получить массив уникальных элементов
		Set<MenuItem> unique = new LinkedHashSet<MenuItem>(items);
		items = new ArrayList<MenuItem>(unique);
		items.add(new MenuItem(true));
	}
	catch (SQLException e)
	{
		e.printStackTrace();
	}
}

/**
 * Загрузка фоток для аудита
 */
public void loadPhotos()
{
	headerPhotos.removeAllViews();
	// список фоток
	List<AuditPhoto> photoList;
	try
	{
		imageDao = getDbHelper().getAuditPhotoDao();
		photoList = imageDao.queryBuilder().where().eq(AuditPhoto.AUDIT_COLUMN_NAME, audit).query();
		if (photoList.size() > 0)
		{
			int i = 0;
			// проходимся по каждой найденной фотке
			for (AuditPhoto photo : photoList)
			{
				i++;
				TextView photoButton = new TextView(this);
				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(90, 35);
				photoButton.setTextAppearance(this, R.style.HyperLink);
				SpannableString content = new SpannableString("Фото-" + i);
				content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
				photoButton.setText(content);
				if (i == 1)
				{
					params.setMargins(0, 10, 0, 0);
				}
				else
				{
					params.setMargins(90 * (i - 1), -35, 0, 0);
				}
				photoButton.setTag(photo);
				photoButton.setLayoutParams(params);
				photoButton.setOnClickListener(viewPhotoListener);
				headerPhotos.addView(photoButton);
			}
			makePhotosCount = photoList.size();
		}
		else
		{
			makePhotosCount = 0;
		}
		String photosCountText = "Вы загрузили " + makePhotosCount + " фото";
		photosCount.setText(photosCountText);
	}
	catch (SQLException e)
	{
		e.printStackTrace();
	}


}

private void updatePointsTable()
{
	pointsTable.removeAllViews();

	TableRow headerRow = new TableRow(this);

	TextView yellowTextView = (TextView) getLayoutInflater().inflate(R.layout.table_header_text, headerRow, false);
	yellowTextView.setText("Месяц");
	headerRow.addView(yellowTextView);

	yellowTextView = (TextView) getLayoutInflater().inflate(R.layout.table_header_text, headerRow, false);
	yellowTextView.setText("1-я проверка");
	headerRow.addView(yellowTextView);

	yellowTextView = (TextView) getLayoutInflater().inflate(R.layout.table_header_text, headerRow, false);
	yellowTextView.setText("2-я проверка");
	headerRow.addView(yellowTextView);

	yellowTextView = (TextView) getLayoutInflater().inflate(R.layout.table_header_text, headerRow, false);
	yellowTextView.setText("ИТОГО баллов");
	headerRow.addView(yellowTextView);

	yellowTextView = (TextView) getLayoutInflater().inflate(R.layout.table_header_text, headerRow, false);
	yellowTextView.setText("Приз выдан");
	headerRow.addView(yellowTextView);

	pointsTable.addView(headerRow);

	try
	{
		Dao<MonthPoints, Integer> monthPointsDao = getDbHelper().getMonthPointsDao();

		Iterator<MonthPoints> iterator = monthPointsDao.queryBuilder().where().eq(MonthPoints.OUTLET_COLUMN_NAME,
		                                                                          audit.outlet).iterator();

		while (iterator.hasNext())
		{
			MonthPoints points = iterator.next();

			TableRow row = new TableRow(this);

			TextView month = (TextView) getLayoutInflater().inflate(R.layout.cell_text, row, false);
			month.setText(points.month.toString());
			row.addView(month);

			TextView firstPoints = (TextView) getLayoutInflater().inflate(R.layout.cell_text, row, false);
			firstPoints.setText(points.firstPoints != null ? "" + points.firstPoints : "—");
			row.addView(firstPoints);

			TextView secondPoints = (TextView) getLayoutInflater().inflate(R.layout.cell_text, row, false);
			secondPoints.setText(points.secondPoints != null ? "" + points.secondPoints : "—");
			row.addView(secondPoints);

			TextView totalPoints = (TextView) getLayoutInflater().inflate(R.layout.cell_text, row, false);
			totalPoints.setText(points.totalPoints != null ? "" + points.totalPoints : "0");
			row.addView(totalPoints);

			if (points.prizeGiven)
			{
				final ImageView image = (ImageView) getLayoutInflater().inflate(R.layout.cell_image, row, false);
				image.setImageResource(R.drawable.checked_icon_green);
				row.addView(image);
			}
			else
			{
				row.addView(getLayoutInflater().inflate(R.layout.cell_empty, row, false));
			}

			pointsTable.addView(row);
		}
	}
	catch (SQLException e)
	{
		message = "Возникла ошибка во время считывания данных с устройства.";

		Log.e(getClass().getName(), message, e);

		showDialog(ERROR_DIALOG_ID);
	}
}

private void updateLayout()
{
	outletName.setText(audit.outlet.name);
	outletAddress.setText(audit.outlet.address);

	outletNotExistCheckBox.setChecked(audit.notExist);
	prizeDeliveredCheckBox.setChecked(audit.prize);

	dataSyncedLabel.setVisibility((audit.synced && audit.startDate != null) ? View.VISIBLE : View.GONE);
	dataNotSyncedWarning.setVisibility(audit.synced ? View.GONE : View.VISIBLE);

	if (needRunWizard)
	{
		prizeDeliveredCheckBox.setVisibility(View.GONE);
		listAdapter = new FastMenuAdapter(this, R.layout.fast_menu_list_item, new ArrayList<MenuItem>());
		bottomRedHeaderWrapper.setVisibility(View.GONE);

		if (outletNotExists)
		{
			startAuditButton.setVisibility(View.GONE);
			pointsTableWrapper.setVisibility(View.GONE);

		}
		else
		{
			startAuditButton.setVisibility(View.VISIBLE);
			pointsTableWrapper.setVisibility(View.VISIBLE);
		}
	}
	else
	{
		startAuditButton.setVisibility(View.GONE);

		if (outletNotExists)
		{
			listAdapter = new FastMenuAdapter(this, R.layout.fast_menu_list_item, new ArrayList<MenuItem>());

			prizeDeliveredCheckBox.setVisibility(View.GONE);
			pointsTableWrapper.setVisibility(View.GONE);
			bottomRedHeaderWrapper.setVisibility(View.GONE);
		}
		else
		{
			listAdapter = new FastMenuAdapter(this, R.layout.fast_menu_list_item, items);

			prizeDeliveredCheckBox.setVisibility(View.VISIBLE);
			pointsTableWrapper.setVisibility(View.VISIBLE);
			bottomRedHeaderWrapper.setVisibility(View.VISIBLE);
		}
	}

	listView.setAdapter(listAdapter);
}

// ----------------------------------------------------------------------
// Protected methods
// ----------------------------------------------------------------------

public void getGPSPoint()
{
	GPSPoint point = GPSUtils.getInstance().getCurrentLocation(this);
	if (point != null)
	{
		Toast.makeText(this, "Location: " + String.valueOf(point.getLatitude()), Toast.LENGTH_SHORT);
	}
}

public void onCreate(Bundle savedInstanceState)
{
	super.onCreate(savedInstanceState);
	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

	try
	{
		startDataUploadService();
	}
	catch (SQLException e)
	{
		e.printStackTrace();
	}

	LinearLayout listViewHeader = (LinearLayout) getLayoutInflater().inflate(R.layout.audit_header, null);
	listView.addHeaderView(listViewHeader, null, false);
	listView.setOnItemClickListener(itemClickListener);

	outletName = (TextView) listViewHeader.findViewById(R.id.outletName);
	outletAddress = (TextView) listViewHeader.findViewById(R.id.outletAddress);

	pointsTable = (TableLayout) listViewHeader.findViewById(R.id.pointsTable);
	pointsTableWrapper = (LinearLayout) listViewHeader.findViewById(R.id.pointsTableWrapper);
	bottomRedHeaderWrapper = (LinearLayout) listViewHeader.findViewById(R.id.bottomRedHeaderWrapper);

	takePhotoButton = (Button) listViewHeader.findViewById(R.id.takePhotoButton);
	startAuditButton = (Button) listViewHeader.findViewById(R.id.startAuditButton);

	outletNotExistCheckBox = (CheckBox) listViewHeader.findViewById(R.id.outletNotExistCheckBox);
	prizeDeliveredCheckBox = (CheckBox) listViewHeader.findViewById(R.id.prizeDeliveredCheckBox);

	photosCount = (TextView) listViewHeader.findViewById(R.id.photosCount);
	headerPhotos = (LinearLayout) listViewHeader.findViewById(R.id.header_photos);

	dataSyncedLabel = (TextView) listViewHeader.findViewById(R.id.dataSyncedLabel);
	dataNotSyncedWarning = (TextView) listViewHeader.findViewById(R.id.dataNotSyncedWarning);
}

@Override
protected void onResume()
{
	super.onResume();

	loadAuditInformation();
	//makePhotosCount = 0;
	loadPhotos();

	updatePointsTable();
	updateLayout();
	getGPSPoint();

	registerReceiver(broadcastReceiver, new IntentFilter(DataUploadService.DATA_SYNCED_EVENT));
}

@Override
protected void onPause()
{
	super.onPause();

	unregisterReceiver(broadcastReceiver);
}

@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data)
{
	if (requestCode == AuditActivity.CAMERA_REQUEST)
	{
		if (resultCode == Activity.RESULT_OK)
		{
			// получаем временный файл изображения
			try
			{
				// создаем битмап из файла и сохраняем его на карту в папку приложения
				Bitmap captureBmp = MediaStore.Images.Media.getBitmap(getContentResolver(),
				                                                      ImageUtils.getInstance()
				                                                                .getTempUri(AuditActivity.this));

				String filename = ImageUtils.getInstance().saveCameraImage(captureBmp);
				String systemPath = ImageUtils.getInstance().getImageStoragePath() + filename;
				AuditPhoto img = new AuditPhoto(audit, filename, systemPath, false);
				imageDao = getDbHelper().getAuditPhotoDao();
				imageDao.create(img);
				//invalidateAudit(audit);
				Toast.makeText(this, "Фотография сохранена", Toast.LENGTH_SHORT).show();
				makePhotosCount += 1;
				//				String photosCountText = "Вы загрузили " + makePhotosCount + " фото:";
				//				photosCount.setText(photosCountText);

				//				TextView photoButton = new TextView(this);
				//				LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(90, 35);
				//				photoButton.setTextAppearance(this, R.style.HyperLink);
				//				SpannableString content = new SpannableString("Фото-" + makePhotosCount);
				//				content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
				//				photoButton.setText(content);
				//				if (makePhotosCount == 1)
				//				{
				//					params.setMargins(0, 10, 0, 0);
				//				}
				//				else
				//				{
				//					params.setMargins(90 * (makePhotosCount - 1), -35, 0, 0);
				//				}
				//				photoButton.setTag(img);
				//				photoButton.setLayoutParams(params);
				//				photoButton.setOnClickListener(viewPhotoListener);
				//				headerPhotos.addView(photoButton);
			}
			catch (FileNotFoundException e)
			{
				e.printStackTrace();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}

protected View.OnClickListener viewPhotoListener = new View.OnClickListener()
{
	public void onClick(View view)
	{
		TextView sender = (TextView) view;
		Intent viewPhotoIntent = new Intent(AuditActivity.this, ViewPhotoActivity.class);
		AuditPhoto photo = (AuditPhoto) sender.getTag();
		viewPhotoIntent.putExtra("photo_id", photo.id);
		viewPhotoIntent.putExtra("photo_type", "audit");
		startActivity(viewPhotoIntent);
	}
};

/**
 * Тач на элемент в списке быстрой навигации.
 */
protected ListView.OnItemClickListener itemClickListener = new ListView.OnItemClickListener()
{

	public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
	{
		//TODO: переход по быстрой навигации
		Intent assortimentIntent = new Intent(AuditActivity.this, AuditFormActivity.class);

		MenuItem clickItem = items.get(i - listView.getHeaderViewsCount());
		if (clickItem.getIsBrand())
		{
			assortimentIntent.putExtra("is_brand", true);
		}
		else
		{
			assortimentIntent.putExtra("kind_id", clickItem.getKindId());
			assortimentIntent.putExtra("type_id", clickItem.getTypeId());
		}
		assortimentIntent.putExtra("audit_id", audit.id);
		assortimentIntent.putExtra("prev_activity", "audit");

		startActivity(assortimentIntent);
	}
};

// ----------------------------------------------------------------------
// Protected methods
// ----------------------------------------------------------------------

public void backToRouteScreen_onClick(View view)
{
	Intent intent = new Intent(this, RouteActivity.class);
	startActivity(intent);
	finish();

	/*if (audit.synced && getIntent().hasExtra("prev_activity") && getIntent().getStringExtra("prev_activity")
			                                                             .equals("route"))
	{
		onBackPressed();
	}
	else
	{
		Intent intent = new Intent(this, RouteActivity.class);
		startActivity(intent);
		finish();
	}*/
}

public void outletNotExistCheckBox_onClick(View view)
{
	setOutletNotExists(outletNotExistCheckBox.isChecked());
}

public void prizeDeliveredCheckBox_onClick(View view)
{
	setPrizeDelivered(prizeDeliveredCheckBox.isChecked());
}

public void startAuditButton_onClick(View view)
{
	try
	{
		Dao<Audit, Integer> auditDao = getDbHelper().getAuditDao();

		audit.startDate = new Date();
		auditDao.update(audit);

		invalidateAudit(audit);
	}
	catch (SQLException e)
	{
		message = "Ошибка записи данных на устройство.";
		showDialog(ERROR_DIALOG_ID);

		Log.e(getClass().getName(), message, e);
	}

	//Запуск мастера.
	Intent assortimentIntent = new Intent(AuditActivity.this, AuditFormActivity.class);
	assortimentIntent.putExtra("audit_id", audit.id);
	assortimentIntent.putExtra("kind_id", 1);
	assortimentIntent.putExtra("type_id", 1);
	assortimentIntent.putExtra("prev_activity", "audit");
	assortimentIntent.putExtra("wizard_mode", true);
	startActivity(assortimentIntent);
}

protected View.OnClickListener takePhotoListener = new View.OnClickListener()
{
	@Override
	public void onClick(View view)
	{
		// создаем интент для производства фотографии с камеры
		if (makePhotosCount >= MAX_PHOTOS_COUNT)
		{
			Toast.makeText(getBaseContext(), "Слишком много фотографий", Toast.LENGTH_SHORT).show();
		}
		else
		{
			Intent makeImageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			makeImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, ImageUtils.getInstance().getTempUri(AuditActivity.this));
			// стартуем интент с возвращаемым результатом
			startActivityForResult(makeImageIntent, CAMERA_REQUEST);
		}
	}
};

public void takePhotoButton_onClick(View view)
{
	// создаем интент для производства фотографии с камеры
	if (makePhotosCount >= MAX_PHOTOS_COUNT)
	{
		Toast.makeText(this, "Слишком много фотографий", Toast.LENGTH_SHORT).show();
	}
	else
	{
		Intent makeImageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		makeImageIntent.putExtra(MediaStore.EXTRA_OUTPUT, ImageUtils.getInstance().getTempUri(AuditActivity.this));
		// стартуем интент с возвращаемым результатом
		startActivityForResult(makeImageIntent, CAMERA_REQUEST);
	}

}
}