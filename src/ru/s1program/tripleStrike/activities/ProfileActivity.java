package ru.s1program.tripleStrike.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import ru.s1program.tripleStrike.R;
import ru.s1program.tripleStrike.tasks.ProfileUpdateTask;
import ru.s1program.tripleStrike.vos.ActionVO;
import ru.s1program.tripleStrike.vos.ErrorVO;

import java.sql.SQLException;

@ContentView(R.layout.profile)
public class ProfileActivity extends TripleStrikeActivity
{

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

@InjectView(R.id.firstNameTextInput)
private EditText firstNameTextInput;

@InjectView(R.id.lastNameTextInput)
private EditText lastNameTextInput;

@InjectView(R.id.patronymicTextInput)
private EditText patronymicTextInput;

// ----------------------------------------------------------------------
// Private methods
// ----------------------------------------------------------------------

private void doUpdateProfile(String firstName, String lastName, String patronymic)
{
	title = "Синхронизация";
	message = "Обновление информации о пользователе на сервере…";
	showDialog(PROGRESS_DIALOG_ID);

	new ProfileUpdateTask(this).execute(firstName, lastName, patronymic);
}

// ----------------------------------------------------------------------
// Protected methods
// ----------------------------------------------------------------------

@Override
protected void onCreate(Bundle savedInstanceState)
{
	super.onCreate(savedInstanceState);
	setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

    try {
        startDataUploadService();
    } catch (SQLException e) {
        e.printStackTrace();
    }
}

@Override
protected void onResume()
{
	super.onResume();

	if (!isLoggedIn())
	{
		Intent intent = new Intent(this, LoginActivity.class);
		startActivity(intent);
	}
}

// ----------------------------------------------------------------------
// Event handlers
// ----------------------------------------------------------------------

public void updateProfileButton_onClick(View view)
{
	final String firstName = firstNameTextInput.getText().toString();
	final String lastName = lastNameTextInput.getText().toString();
	final String patronymic = patronymicTextInput.getText().toString();


	if (lastName.length() <= 0)
	{
		message = "Укажите вашу фамилию.";
		requestFocusView = lastNameTextInput;
		showDialog(WARN_DIALOG_ID);
		return;
	}
	else if (firstName.length() <= 0)
	{
		message = "Укажите ваше имя.";
		requestFocusView = firstNameTextInput;
		showDialog(WARN_DIALOG_ID);
		return;
	}
	else if (patronymic.length() <= 0)
	{
		message = "Укажите ваше отчество.";
		requestFocusView = patronymicTextInput;
		showDialog(WARN_DIALOG_ID);
		return;
	}

	doUpdateProfile(firstName, lastName, patronymic);
}

@Override
public void asyncTask_onExecuted(ActionVO actionVO)
{
	super.asyncTask_onExecuted(actionVO);

	removeDialog(PROGRESS_DIALOG_ID);

	if (actionVO.getStatus() == 0 || actionVO.getResult() == 0)
	{
		if (actionVO.getErrors() != null && actionVO.getErrors().size() > 0)
		{
			message = "";
			for (ErrorVO errorVO : actionVO.getErrors())
			{
				message += errorVO.message;
			}
		}
		else
		{
			message = "Ошибка обработки запроса сервером.";
		}

		showDialog(ERROR_DIALOG_ID);
	}
	else
	{
		Intent intent = new Intent(this, RouteActivity.class);
		startActivity(intent);
	}
}

}