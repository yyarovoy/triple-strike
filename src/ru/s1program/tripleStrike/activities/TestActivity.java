package ru.s1program.tripleStrike.activities;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.j256.ormlite.dao.Dao;
import org.json.JSONException;
import org.json.JSONObject;
import ru.s1program.tripleStrike.R;
import ru.s1program.tripleStrike.net.FileUploader;
import ru.s1program.tripleStrike.orm.account.Session;
import ru.s1program.tripleStrike.orm.route.Route;
import ru.s1program.tripleStrike.orm.route.audit.Audit;
import ru.s1program.tripleStrike.orm.route.audit.AuditPhoto;
import ru.s1program.tripleStrike.orm.route.outlet.Outlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


public class TestActivity extends TripleStrikeActivity {
    private Boolean isDataUploading = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        try {
            startDataUploadService();
        } catch (SQLException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    protected void startDataUploadService() throws SQLException {
        final Dao<AuditPhoto, Integer> auditPhotoDao = getDbHelper().getAuditPhotoDao();
        final List<AuditPhoto> auditPhotos = auditPhotoDao.queryBuilder()
                //.where()
//                .eq(AuditPhoto.SYNCED_COLUMN_NAME, false)
                .query();

        if (auditPhotos.size() > 0)
        {
            for (AuditPhoto auditPhoto : auditPhotos)
            {
                auditPhotoDao.refresh(auditPhoto);

                synchronizeAuditPhoto(auditPhoto);
            }
        }

    }

    private void synchronizeAuditPhoto(AuditPhoto auditPhoto) throws SQLException {
        Session session = resumeSession();
        final Route route = resumeRoute();
        if (isDataUploading)
        {
            return;
        }

        if (auditPhoto.path == null)
        {
            return;
        }

        FileUploader uploader = new FileUploader();

        try
        {
            Log.d(getClass().getName(), "Отправка файла " + auditPhoto.path);

            isDataUploading = true;
            Dao<Audit,Integer> auditDao = getDbHelper().getAuditDao();
            auditDao.refresh(auditPhoto.audit);
            Dao<Outlet,Integer> outletDao = getDbHelper().getOutletDao();
            outletDao.refresh(auditPhoto.audit.outlet);
            String urlWithParams = "http://api.s1program.ru/upload/?uid="
                    + session.uid + "&sid="+session.sid+"&route="+route.id
                    +"&tt="+auditPhoto.audit.outlet.id+"&kind=2&lat=20&lng=20";

            String responseString = uploader.executeMultipartPost(urlWithParams,auditPhoto.path,auditPhoto.filename);
            //String responseString = uploader.upload(urlWithParams, auditPhoto.path,auditPhoto.filename);
            JSONObject jsonObject = new JSONObject(responseString);
            JSONObject errorString = jsonObject.getJSONObject("error");
            String desc = errorString.getString("desc");
            Toast.makeText(this,desc,Toast.LENGTH_SHORT).show();
            Log.d(getClass().getName(), "Ответ: " + responseString);
        }
        catch (IOException e)
        {
            Log.e(getClass().getName(), "Ошибка отправки файла", e);
        } catch (JSONException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        } finally
        {
            isDataUploading = false;
        }
    }
}
