package ru.s1program.tripleStrike.vos;

public enum ErrorCode
{

// ----------------------------------------------------------------------
// Enumerations
// ----------------------------------------------------------------------

	UNHANDLED_EXCEPTION(0, "Unhandled Exception"),

	REQUEST_INVALID(103, "JSON-строка в теле запроса не валидная."),
	REQUEST_UNKNOWN_API_VERSION(104, "В запросе указана не известная версия API."),
	REQUEST_UNKNOWN_ACTION(106, "Не известное действие (значение параметра action)."),

	ACCOUNT_DATA_INVALID(201, "Не корректные данные пользователя"),
	ACCOUNT_NOT_REGISTERED(203, "Пользователь с указанными данными не зарегистрирован."),
	ACCOUNT_PHONE_INCORRECT(207, "Не корректный номер телефона"),
	ACCOUNT_LOGIN_INCORRECT(208, "Не корректный логин пользователя"),
	ACCOUNT_PASSWORD_INCORRECT(209, "Указан не корректный пароль"),
	ACCOUNT_LOGIN_OR_PASSWORD_INCORRECT(210, "Не правильно указан логин или пароль."),
	ACCOUNT_FIRST_NAME_INCORRECT(211, "Не корректное имя пользователя"),
	ACCOUNT_LAST_NAME_INCORRECT(212, "Не корректная фамилия пользователя"),
	ACCOUNT_PATRONYMIC_INCORRECT(214, "Не корректное отчество пользователя"),

	SESSION_INVALID(250, "Cессия"),
	SESSION_NOT_FULLY_SPECIFIED(251, "Не переданы параметры сессии участника / или переданы не полностью."),
	SESSION_NOT_EXIST(252,
	                  "Cессии с указанными параметрами не существует, либо она устарела. Пользователю необходимо авторизоваться."),

	ROUTE_DATA_INVALID(810, "В запросе отсутствуют данные по маршруту."),
	ROUTE_NOT_EXISTS(811, "Указанный маршрут не найден."),
	ROUTE_VERSION_OBSOLETE(812,
	                       "Версия обновления старее или идентична текущей версии на сервере. Обновление отклонено."),
	ROUTE_OUTLET_NOT_EXISTS(814, "Торговая точка с ID N не найдена."),

	CONNECTION_NOT_ESTABLISHED(1001, "Ошибка соединения с сервером.\nПовторите попытку позже."),

	SQLITE_EXCEPTION(3000, "Ошибка при сохранении данных маршрута на устройстве."),

	RESPONSE_INVALID(3001, "Неправильный формат ответа сервера."),
	RESPONSE_ENCODING_INVALID(1100, "Неправильная кодировка символов в сообщениях, получаемых сервера."),
	RESPONSE_ROUTE_DATA_INVALID(3002, "В полученных от сервера данных отсутствует описание маршрута."),

	SESSION_EXPIRED(3003, "Не удается возобновить сессию пользователя на устройстве.");

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private final Integer code;
private final String description;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

private ErrorCode(Integer code, String description)
{
	this.code = code;
	this.description = description;
}

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

public static ErrorCode fromCode(Integer code)
{
	for (ErrorCode errorCode : values())
	{
		if (errorCode.code.equals(code))
		{
			return errorCode;
		}
	}

	return null;
}

@Override
public String toString()
{
	return description;
}
}
