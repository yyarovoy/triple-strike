package ru.s1program.tripleStrike.vos;

public enum ActionType
{
	LOGIN("login"),
	LOGOUT("logout"),
	UPDATE_PROFILE("update_profile"),
	GET_ROUTE("get_route"),
	SAVE_AUDIT("save_audit"),
	UPLOAD("upload"),
	PING_AUDIT("ping_audit");

private String text;

ActionType(String text)
{
	this.text = text;
}

public static ActionType fromString(String text)
{
	if (text != null)
	{
		for (ActionType type : ActionType.values())
		{
			if (text.equalsIgnoreCase(type.text))
			{
				return type;
			}
		}
	}
	return null;
}

@Override
public String toString()
{
	return text;
}
}
