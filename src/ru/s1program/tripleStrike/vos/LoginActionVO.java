package ru.s1program.tripleStrike.vos;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoginActionVO extends ActionVO
{

// ----------------------------------------------------------------------
// Public props
// ----------------------------------------------------------------------

public String login;
public String password;
public String phone;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public LoginActionVO(JSONObject json) throws JSONException
{
	super(json);
}

public LoginActionVO(String login, String password, String phone, int status, int result, ArrayList<ErrorVO> errors)
{
	super(ActionType.LOGIN, status, result, errors);

	this.login = login;
	this.password = password;
	this.phone = phone;
}

}
