package ru.s1program.tripleStrike.vos;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class RouteGetActionVO extends ActionVO
{

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public RouteGetActionVO(JSONObject jsonObject) throws JSONException
{
	super(jsonObject);
}

public RouteGetActionVO(int status, int result, ArrayList<ErrorVO> errors)
{
	super(ActionType.GET_ROUTE, status, result, errors);
}

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

@Override
public void parseJson(JSONObject response) throws JSONException
{
	super.parseJson(response);
}
}
