package ru.s1program.tripleStrike.vos;

import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class ActionVO
{

// ----------------------------------------------------------------------
// Protected props
// ----------------------------------------------------------------------

protected JSONObject source;

protected ActionType type;

protected int status;
protected int result;

protected SessionVO session;

protected JSONObject data;

protected ArrayList<ErrorVO> errors = null;

// ----------------------------------------------------------------------
// Getters and setters
// ----------------------------------------------------------------------

public JSONObject getSource()
{
	return source;
}

public ActionType getType()
{
	return type;
}

public int getStatus()
{
	return status;
}

public int getResult()
{
	return result;
}

public SessionVO getSession()
{
	return session;
}

public JSONObject getData()
{
	return data;
}

public ArrayList<ErrorVO> getErrors()
{
	return errors;
}

// ----------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------

public ActionVO()
{
}

public ActionVO(ActionType type)
{
	this.type = type;
}

public ActionVO(JSONObject json) throws JSONException
{
	parseJson(json);
}

public ActionVO(ActionType type, int status, int result, ArrayList<ErrorVO> errors)
{
	this.type = type;
	this.status = status;
	this.result = result;
	this.errors = errors;
}

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

public void parseJson(JSONObject response) throws JSONException
{
	this.source = response;

	type = ActionType.fromString(response.getString("action"));
	status = response.getInt("status");
	result = response.getInt("result");

	if (response.has("session"))
	{
		final JSONObject sessionJson = response.getJSONObject("session");

		session = new SessionVO(sessionJson.getString("uid"),
		                        sessionJson.getString("sid"),
		                        new Date(sessionJson.getLong("expiration") * 1000));
	}

	if (response.has("error"))
	{
		errors = new ArrayList<ErrorVO>();

		final JSONArray jsonArray = response.getJSONArray("error");
		int code;
		String message;

		for (int i = 0; i < jsonArray.length(); i++)
		{
			JSONObject errorJson = jsonArray.getJSONObject(i);

			if (errorJson.has("code") && errorJson.has("desc"))
			{
				code = errorJson.getInt("code");
				message = errorJson.getString("desc");

				Log.d(getClass().getName(), "Add error code: " + code + ", message: " + message);

				errors.add(new ErrorVO(ErrorCode.fromCode(code)));
			}
		}
	}

	if (response.has("data"))
	{
		data = response.getJSONObject("data");
	}
}

}