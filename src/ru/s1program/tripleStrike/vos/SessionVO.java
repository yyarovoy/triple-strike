package ru.s1program.tripleStrike.vos;

import java.util.Date;

public class SessionVO {

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private String uid;
private String sid;
private Date expirationDate;

// ----------------------------------------------------------------------
// Getters and setters
// ----------------------------------------------------------------------

public String getUid()
{
	return uid;
}

public String getSid()
{
	return sid;
}

public Date getExpirationDate()
{
	return expirationDate;
}

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

public SessionVO(String uid, String sid, Date expirationDate)
{
	this.uid = uid;
	this.sid = sid;
	this.expirationDate = expirationDate;
}
}
