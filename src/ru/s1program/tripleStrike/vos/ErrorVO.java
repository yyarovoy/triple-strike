package ru.s1program.tripleStrike.vos;

public class ErrorVO
{

// ----------------------------------------------------------------------
// Public props
// ----------------------------------------------------------------------

public ErrorCode code;
public String message;

// ----------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------


public ErrorVO(Integer code)
{
	ErrorCode errorCode = ErrorCode.fromCode(code);
	if (errorCode != null)
	{
		this.code = errorCode;
		this.message = errorCode.toString();
	}
}

public ErrorVO(Integer code, String message)
{
	ErrorCode errorCode = ErrorCode.fromCode(code);
	if (errorCode != null)
	{
		this.code = errorCode;
	}

	this.message = message;
}

public ErrorVO(ErrorCode code)
{
	this.code = code;
	this.message = code.toString();
}

public ErrorVO(ErrorCode code, String message)
{
	this.code = code;
	this.message = message;
}
}