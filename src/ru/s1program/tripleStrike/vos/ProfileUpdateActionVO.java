package ru.s1program.tripleStrike.vos;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProfileUpdateActionVO extends ActionVO
{

// ----------------------------------------------------------------------
// Public props
// ----------------------------------------------------------------------

public String firstName;
public String lastName;
public String patronymic;

// ----------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------

public ProfileUpdateActionVO(JSONObject json) throws JSONException
{
	super(json);
}

public ProfileUpdateActionVO(String firstName, String lastName, String patronymic, int status, int result,
                             ArrayList<ErrorVO> errors)
{
	super(ActionType.UPDATE_PROFILE, status, result, errors);

	this.firstName = firstName;
	this.lastName = lastName;
	this.patronymic = patronymic;
}

}
