package ru.s1program.tripleStrike.net;

import android.util.Log;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class HttpRequest
{

DefaultHttpClient httpClient;
HttpContext localContext;
private String ret;

HttpResponse response = null;
HttpPost httpPost = null;
HttpGet httpGet = null;

public HttpRequest()
{
	HttpParams httpParams = new BasicHttpParams();

	HttpConnectionParams.setConnectionTimeout(httpParams, 120000);
	HttpConnectionParams.setSoTimeout(httpParams, 120000);
	httpClient = new DefaultHttpClient(httpParams);
	localContext = new BasicHttpContext();
}

public void clearCookies()
{
	httpClient.getCookieStore()
	          .clear();
}

public void abort()
{
	if (httpClient != null)
	{
		Log.d(getClass().getName(), "Abort connection.");
		httpPost.abort();
	}
}

public String sendPost(String url, String data) throws IOException
{
	return sendPost(url, data, null);
}

public String sendJSONPost(String url, JSONObject data) throws IOException
{
	return sendPost(url, data.toString(), "application/json");
}

public String sendPost(String url, String data, String contentType) throws IOException
{
	ret = null;

	httpClient.getParams()
	          .setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.RFC_2109);

	httpPost = new HttpPost(url);
	response = null;

	StringEntity tmp = null;

	Log.d("Your App Name Here", "Setting httpPost headers");

	httpPost.setHeader("User-Agent", "Triple Strike");
	httpPost.setHeader("Accept",
	                   "text/html,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");

	if (contentType != null)
	{
		httpPost.setHeader("Content-Type", contentType);
	}
	else
	{
		httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
	}

	tmp = new StringEntity(data, "UTF-8");

	httpPost.setEntity(tmp);

	Log.d(getClass().getName(), "REQUEST: " + url + "?" + data);
	response = httpClient.execute(httpPost, localContext);

	if (response != null)
	{
		ret = EntityUtils.toString(response.getEntity());
	}

	Log.d(getClass().getName(), "RESPONSE: " + ret);

	return ret;
}

public String sendGet(String url) throws IOException
{
	httpGet = new HttpGet(url);

	response = httpClient.execute(httpGet);

	//int status = response.getStatusLine().getStatusCode();

	// we assume that the response body contains the error message

	ret = EntityUtils.toString(response.getEntity());

	return ret;
}

public InputStream getHttpStream(String urlString) throws IOException
{
	InputStream in = null;
	int response = -1;

	URL url = new URL(urlString);
	URLConnection conn = url.openConnection();

	if (!(conn instanceof HttpURLConnection))
	{
		throw new IOException("Not an HTTP connection");
	}

	HttpURLConnection httpConn = (HttpURLConnection) conn;
	httpConn.setAllowUserInteraction(false);
	httpConn.setInstanceFollowRedirects(true);
	httpConn.setRequestMethod("GET");
	httpConn.connect();

	response = httpConn.getResponseCode();

	if (response == HttpURLConnection.HTTP_OK)
	{
		in = httpConn.getInputStream();
	}

	return in;
}
}
