package ru.s1program.tripleStrike.net;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ByteArrayBody;
import org.apache.http.impl.client.DefaultHttpClient;

public class FileUploader {

    public static final String LINE_END = "\r\n";
    public static final String TWO_HYPHENS = "--";
    public static final String BOUNDARY = "Asrf456BGe4h";

    HttpURLConnection connection = null;
    DataOutputStream outputStream = null;
    DataInputStream inputStream = null;

    public FileUploader() {
    }

    public String upload(String serverUrl, String path, String name) throws IOException {
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;

        FileInputStream fileInputStream = new FileInputStream(new File(path));

        URL url = new URL(serverUrl);
        connection = (HttpURLConnection) url.openConnection();

        // Allow Inputs & Outputs
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);

        // Enable POST method
        connection.setRequestMethod("POST");

        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("ENCTYPE", "multipart/form-data");

        connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + BOUNDARY);

        outputStream = new DataOutputStream(connection.getOutputStream());
        outputStream.writeBytes(TWO_HYPHENS + BOUNDARY + LINE_END);
        outputStream.writeBytes(
                "Content-Disposition: form-data; name=\"photo\";filename=\"" + name + "\"" +
                        LINE_END);
        //outputStream.writeBytes("Content-Type: image/jpeg"+ LINE_END);
        outputStream.writeBytes(LINE_END);

        bytesAvailable = fileInputStream.available();
        bufferSize = Math.min(bytesAvailable, maxBufferSize);
        buffer = new byte[bufferSize];

        // Read file
        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            outputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
        outputStream.writeBytes(LINE_END);
        outputStream.writeBytes(TWO_HYPHENS + BOUNDARY + TWO_HYPHENS + LINE_END);
        // Responses from the server (code and message)
//	int serverResponseCode = connection.getResponseCode();
        //String serverResponseMessage = connection.getResponseMessage();
        fileInputStream.close();
        outputStream.flush();
        int ch;
        InputStreamReader is = new InputStreamReader(connection.getInputStream(), "UTF-8");
        StringBuffer buff = new StringBuffer();
        while ((ch = is.read()) != -1) {
            buff.append((char) ch);
        }
        outputStream.close();
        return buff.toString();
    }

    public String executeMultipartPost(String serverUrl, String path, String name) throws
            Exception
    {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        Bitmap bm = BitmapFactory.decodeFile(path);
        bm.compress(Bitmap.CompressFormat.JPEG, 75, bos);
        byte[] data = bos.toByteArray();
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost postRequest = new HttpPost(
                serverUrl);
        ByteArrayBody bab = new ByteArrayBody(data, name);
        // File file= new File("/mnt/sdcard/forest.png");
        // FileBody bin = new FileBody(file);
        MultipartEntity reqEntity = new MultipartEntity(
                HttpMultipartMode.BROWSER_COMPATIBLE);
        reqEntity.addPart("photo", bab);
        postRequest.setEntity(reqEntity);
        HttpResponse response = httpClient.execute(postRequest);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                response.getEntity().getContent(), "UTF-8"));
        String sResponse;
        StringBuilder s = new StringBuilder();

        while ((sResponse = reader.readLine()) != null) {
            s = s.append(sResponse);
        }
        return s.toString();
    }

}
