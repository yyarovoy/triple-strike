package ru.s1program.tripleStrike.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AutoStartService extends BroadcastReceiver
{

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private AlarmReceiver alarm = new AlarmReceiver();

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

@Override
public void onReceive(Context context, Intent intent)
{
	if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))
	{
		alarm.setAlarm(context);
	}
}

}
