package ru.s1program.tripleStrike.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

public class AlarmReceiver extends BroadcastReceiver
{

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

@Override
public void onReceive(Context context, Intent intent)
{
	PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "AlarmService");
	wl.acquire();

	Log.d(getClass().getName(), "onReceive");

	Intent serviceIntent = new Intent(context, DataUploadService.class);
	context.startService(serviceIntent);

	wl.release();
}

public void setAlarm(Context context)
{
	final int interval = 1000 * 60 * 5;

	AlarmManager am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	Intent i = new Intent(context, AlarmReceiver.class);
	PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
	am.setRepeating(AlarmManager.RTC_WAKEUP,
	                System.currentTimeMillis() + interval, interval,
	                pi);
}

public void cancelAlarm(Context context)
{
	Intent intent = new Intent(context, AlarmReceiver.class);
	PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
	AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	alarmManager.cancel(sender);
}
}
