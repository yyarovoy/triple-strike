package ru.s1program.tripleStrike.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.CloseableIterator;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.s1program.tripleStrike.net.FileUploader;
import ru.s1program.tripleStrike.net.HttpRequest;
import ru.s1program.tripleStrike.orm.DbOpenHelper;
import ru.s1program.tripleStrike.orm.account.Account;
import ru.s1program.tripleStrike.orm.account.Session;
import ru.s1program.tripleStrike.orm.route.Route;
import ru.s1program.tripleStrike.orm.route.audit.Audit;
import ru.s1program.tripleStrike.orm.route.audit.AuditPhoto;
import ru.s1program.tripleStrike.orm.route.audit.Month;
import ru.s1program.tripleStrike.orm.route.audit.MonthPoints;
import ru.s1program.tripleStrike.orm.route.brand.Brand;
import ru.s1program.tripleStrike.orm.route.brand.BrandResult;
import ru.s1program.tripleStrike.orm.route.outlet.Outlet;
import ru.s1program.tripleStrike.orm.route.unit.Unit;
import ru.s1program.tripleStrike.orm.route.unit.UnitPhoto;
import ru.s1program.tripleStrike.orm.route.unit.UnitResult;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class DataUploadService extends IntentService
{

// ----------------------------------------------------------------------
// Constants
// ----------------------------------------------------------------------

private static final String TAG = "DataUploadService";

public static final String AUDIT_SYNCED_EVENT = "ru.s1program.tripleStrike.services.IntentService.AUDIT_SYNCED_EVENT";
public static final String AUDIT_PHOTO_SYNCED_EVENT = "ru.s1program.tripleStrike.services.IntentService.AUDIT_PHOTO_SYNCED_EVENT";
public static final String UNIT_PHOTO_SYNCED_EVENT = "ru.s1program.tripleStrike.services.IntentService.UNIT_PHOTO_SYNCED_EVENT";
public static final String DATA_SYNCED_EVENT = "ru.s1program.tripleStrike.services.IntentService.DATA_SYNCED_EVENT";

private static final int MAX_SENDING_QUE_LENGTH = 20;
private static final int MAX_FAILURE_COUNT = 3;

// ----------------------------------------------------------------------
// Private props
// ----------------------------------------------------------------------

private DbOpenHelper dbOpenHelper;

private Dao<Audit, Integer> auditDao;
private Dao<AuditPhoto, Integer> auditPhotoDao;

private Dao<Unit, Integer> unitDao;
private Dao<UnitResult, Integer> unitResultDao;
private Dao<UnitPhoto, Integer> unitPhotoDao;

private Dao<Brand, Integer> brandDao;
private Dao<BrandResult, Integer> brandResultDao;

private Dao<Outlet, Integer> outletDao;
private Dao<MonthPoints, Integer> monthPointsDao;

// ----------------------------------------------------------------------
// Constructors
// ----------------------------------------------------------------------

public DataUploadService()
{
	super(TAG);
}

// ----------------------------------------------------------------------
// Private methods
// ----------------------------------------------------------------------

private void initializeDaoFields()
{
	try
	{
		auditDao = getDbHelper().getAuditDao();
		auditPhotoDao = getDbHelper().getAuditPhotoDao();

		unitDao = getDbHelper().getUnitDao();
		unitResultDao = getDbHelper().getUnitResultDao();
		unitPhotoDao = getDbHelper().getUnitPhotoDao();

		brandDao = getDbHelper().getBandDao();
		brandResultDao = getDbHelper().getBandResultDao();

		outletDao = getDbHelper().getOutletDao();
		monthPointsDao = getDbHelper().getMonthPointsDao();
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Ошибка взаимодействия с базой данных", e);
	}
}

private void cancelAlarm()
{
	AlarmReceiver receiver = new AlarmReceiver();
	receiver.cancelAlarm(this);
}

private void prepareNextAlarm()
{
	AlarmReceiver receiver = new AlarmReceiver();
	receiver.setAlarm(this);
}

private void doTick()
{
	Log.d(getClass().getName(), "doTick()");

	if (!isLoggedIn())
	{
		prepareNextAlarm();
		Log.d(getClass().getName(), "Не авторизованы. Засыпаем.");
		return;
	}

	Route route = resumeRoute();
	if (route == null)
	{
		prepareNextAlarm();
		Log.d(getClass().getName(), "Маршрут не найден. Засыпаем.");
		return;
	}

	try
	{
		/*

		final PreparedQuery<AuditPhoto> auditPhotoQuery = auditPhotoDao.queryBuilder()
		                                                               .where()
		                                                               .eq(AuditPhoto.SYNCED_COLUMN_NAME, false)
		                                                               .and()
		                                                               .eq(AuditPhoto.SYNC_FAILED_COLUMN_NAME, false)
		                                                               .prepare();

		final PreparedQuery<UnitPhoto> unitPhotoQuery = unitPhotoDao.queryBuilder()
		                                                            .where()
		                                                            .eq(UnitPhoto.SYNCED_COLUMN_NAME, false)
		                                                            .and()
		                                                            .eq(UnitPhoto.SYNC_FAILED_COLUMN_NAME, false)
		                                                            .prepare();

		Audit audit = auditDao.queryForFirst(auditQuery);
		AuditPhoto auditPhoto = auditPhotoDao.queryForFirst(auditPhotoQuery);
		UnitPhoto unitPhoto = unitPhotoDao.queryForFirst(unitPhotoQuery);*/


		int i = MAX_SENDING_QUE_LENGTH;

		while (--i >= 0)
		{
			Log.d(getClass().getName(), "" + i);

			Audit audit = findNotSyncedAudit(route);
			AuditPhoto auditPhoto = findNotSyncedAuditPhoto(route);
			UnitPhoto unitPhoto = findNotSyncedUnitPhoto(route);
			if (audit != null)
			{
				synchronizeAudit(audit);
			}
			else if (auditPhoto != null)
			{
				synchronizeAuditPhoto(auditPhoto);
			}
			else if (unitPhoto != null)
			{
				synchronizeUnitPhoto(unitPhoto);
			}
			else
			{
				break;
			}
		}
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Ошибка работы с базой данных", e);
	}
	catch (JSONException e)
	{
		Log.e(getClass().getName(), "Ошибка работы с JSON", e);
	}
	finally
	{
		prepareNextAlarm();
	}
}

private int unixTime(Date date)
{
	return (int) (date.getTime() / 1000L);
}

private DbOpenHelper getDbHelper()
{
	if (dbOpenHelper == null)
	{
		dbOpenHelper = OpenHelperManager.getHelper(this, DbOpenHelper.class);
	}
	return dbOpenHelper;
}

private void releaseDbHelper()
{
	if (dbOpenHelper != null)
	{
		OpenHelperManager.releaseHelper();
		dbOpenHelper = null;
	}
}

private Boolean isLoggedIn()
{
	return (resumeSession() != null);
}

private Session resumeSession()
{
	try
	{
		Dao<Session, String> sessionDao = getDbHelper().getSessionDao();

		if (sessionDao.countOf() > 0)
		{
			PreparedQuery<Session> query = sessionDao.queryBuilder()
			                                         .where()
			                                         .eq(Session.ACTIVE_FIELD_NAME, true)
			                                         .and()
			                                         .gt(Session.EXPIRATION_DATE_COLUMN_NAME, new Date())
			                                         .prepare();
			return sessionDao.queryForFirst(query);
		}
		return null;
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Проблема при взаимодействии с локальной базой данных.", e);
	}

	return null;
}

private Account resumeAccount()
{
	final Session actualSession = resumeSession();

	if (resumeSession() == null)
	{
		return null;
	}

	try
	{
		final Dao<Account, String> accountDao = getDbHelper().getAccountDao();

		PreparedQuery<Account> accountQuery = accountDao.queryBuilder().where().eq(Account.SESSION_COLUMN_NAME,
		                                                                           actualSession).prepare();

		return accountDao.queryForFirst(accountQuery);

	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Проблема при считывании данных аккаунта с устройства.", e);
	}

	return null;
}

private Route resumeRoute()
{
	Account actualAccount = resumeAccount();

	if (actualAccount == null)
	{
		return null;
	}

	try
	{
		getDbHelper().getAccountDao().refresh(actualAccount);

		return actualAccount.route;
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Проблема при считывании данных аккаунта с устройства.", e);
	}

	return null;
}

private void dispatchAuditSyncedEvent(Audit audit)
{
	Intent intent = new Intent(AUDIT_SYNCED_EVENT);
	intent.putExtra("audit_id", audit.id);
	sendBroadcast(intent);
}

private void dispatchAuditPhotoSyncedEvent(AuditPhoto auditPhoto)
{
	Intent intent = new Intent(AUDIT_PHOTO_SYNCED_EVENT);
	intent.putExtra("audit_photo_id", auditPhoto.id);
	sendBroadcast(intent);
}

private void dispatchUnitPhotoSyncedEvent(UnitPhoto unitPhoto)
{
	Intent intent = new Intent(UNIT_PHOTO_SYNCED_EVENT);
	intent.putExtra("unit_photo_id", unitPhoto.id);
	sendBroadcast(intent);
}

private void dispatchDataSyncedEvent()
{
	sendBroadcast(new Intent(DATA_SYNCED_EVENT));
}

private Audit findNotSyncedAudit(Route route) throws SQLException
{
	final QueryBuilder<Audit, Integer> auditQueryBuilder = auditDao.queryBuilder();

	Where<Audit, Integer> where = auditQueryBuilder.where();
	where.and(where.eq(Audit.ROUTE_FIELD_NAME, route),
	          where.eq(Audit.SYNCED_COLUMN_NAME, false),
	          where.or(where.eq(Audit.SYNC_FAILED_COLUMN_NAME, false), where.lt(Audit.SYNC_FAILURE_COUNT_COLUMN_NAME,
	                                                                            MAX_FAILURE_COUNT)));

	final PreparedQuery<Audit> auditQuery = auditQueryBuilder.prepare();

	return auditDao.queryForFirst(auditQuery);
}

private AuditPhoto findNotSyncedAuditPhoto(Route route) throws SQLException
{
	final QueryBuilder<AuditPhoto, Integer> auditQueryBuilder = auditPhotoDao.queryBuilder();

	Where<AuditPhoto, Integer> where = auditQueryBuilder.where();
	where.and(where.eq(AuditPhoto.SYNCED_COLUMN_NAME, false), where.or(where.eq(AuditPhoto.SYNC_FAILED_COLUMN_NAME,
	                                                                            false),
	                                                                   where.lt(AuditPhoto.SYNC_FAILURE_COUNT_COLUMN_NAME,
	                                                                            MAX_FAILURE_COUNT)));

	final PreparedQuery<AuditPhoto> auditPhotoQuery = auditQueryBuilder.prepare();

	List<AuditPhoto> auditPhotoList = auditPhotoDao.query(auditPhotoQuery);

	for (AuditPhoto photo : auditPhotoList)
	{
		auditPhotoDao.refresh(photo);

		Audit audit = photo.audit;
		auditDao.refresh(audit);

		if (audit.route.id.equals(route.id))
		{
			return photo;
		}
	}

	return null;
}

private UnitPhoto findNotSyncedUnitPhoto(Route route) throws SQLException
{
	final QueryBuilder<UnitPhoto, Integer> queryBuilder = unitPhotoDao.queryBuilder();

	Where<UnitPhoto, Integer> where = queryBuilder.where();
	where.and(where.eq(UnitPhoto.SYNCED_COLUMN_NAME, false), where.or(where.eq(UnitPhoto.SYNC_FAILED_COLUMN_NAME,
	                                                                           false),
	                                                                  where.lt(UnitPhoto.SYNC_FAILURE_COUNT_COLUMN_NAME,
	                                                                           MAX_FAILURE_COUNT)));

	final PreparedQuery<UnitPhoto> unitPhotoQuery = queryBuilder.prepare();

	List<UnitPhoto> unitPhotoList = unitPhotoDao.query(unitPhotoQuery);

	for (UnitPhoto photo : unitPhotoList)
	{
		unitPhotoDao.refresh(photo);

		Audit audit = photo.audit;
		auditDao.refresh(audit);

		if (audit.route.id.equals(route.id))
		{
			return photo;
		}
	}

	return null;
}

private JSONObject createRequestJson(Session session, Route route, Audit audit) throws JSONException, SQLException
{
	final JSONObject jsonObject = new JSONObject();

	final JSONObject request = new JSONObject();
	request.put("version", "1.0");
	request.put("action", "save_audit");
	request.put("session", createSessionJson(session));

	final JSONObject jsonRoute = createRouteJson(route, audit);

	final JSONObject data = new JSONObject();
	data.put("route", jsonRoute);
	request.put("data", data);

	jsonObject.put("request", request);

	return jsonObject;
}

private JSONObject createSessionJson(Session session) throws JSONException
{
	final JSONObject jsonSession = new JSONObject();
	jsonSession.put("uid", session.uid);
	jsonSession.put("sid", session.sid);

	return jsonSession;
}

private JSONObject createRouteJson(Route route, Audit audit) throws SQLException, JSONException
{
	final JSONObject jsonAudit = createAuditJson(audit);

	final JSONObject tt = new JSONObject();
	tt.put(audit.outlet.id + "", jsonAudit);

	final JSONObject jsonRoute = new JSONObject();

	final int routeVersion = route.serverVersion + 10;
	jsonRoute.put("id", route.serverId);
	jsonRoute.put("version", routeVersion);
	jsonRoute.put("tt", tt);

	return jsonRoute;
}

private JSONObject createAuditJson(Audit audit) throws SQLException, JSONException
{
	final JSONObject list = createUnitsJson(audit.unitResults);
	final JSONObject brand = createBrandsJson(audit.brandResults);

	JSONObject auditInner = new JSONObject();
	auditInner.put("start_date", (audit.startDate != null) ? unixTime(audit.startDate) : 0);
	auditInner.put("last_updated", (audit.lastUpdated != null) ? unixTime(audit.lastUpdated) : 0);
	auditInner.put("not_exist", audit.notExist ? 1 : 0);
	auditInner.put("list", list);
	auditInner.put("brand", brand);

	final JSONObject jsonAudit = new JSONObject();
	jsonAudit.put("prize", audit.prize ? 1 : 0);
	jsonAudit.put("audit", auditInner);

	return jsonAudit;
}

private JSONObject createBrandsJson(ForeignCollection<BrandResult> brandResults) throws SQLException, JSONException
{
	final JSONObject brands = new JSONObject();

	CloseableIterator<BrandResult> brandResultsIterator = brandResults.closeableIterator();
	try
	{
		while (brandResultsIterator.hasNext())
		{
			BrandResult brandResult = brandResultsIterator.next();
			brandResultDao.refresh(brandResult);

			Brand brand = brandResult.brand;
			brandDao.refresh(brand);

			brands.put("" + brand.id, brandResult.result != null ? brandResult.result : 0);
		}
	}
	finally
	{
		brandResultsIterator.close();
	}

	return brands;
}

private JSONObject createUnitsJson(ForeignCollection<UnitResult> unitResults) throws JSONException, SQLException
{
	final JSONObject list = new JSONObject();

	final CloseableIterator<UnitResult> iterator = unitResults.closeableIterator();
	try
	{
		while (iterator.hasNext())
		{
			UnitResult unitResult = iterator.next();
			unitResultDao.refresh(unitResult);

			Unit unit = unitResult.unit;
			unitDao.refresh(unit);

			list.put("" + unit.id, unitResult.result != null ? unitResult.result : 0);
		}
	}
	finally
	{
		iterator.close();
	}

	return list;
}

private void updateAuditFromJsonString(Audit audit, String jsonString) throws JSONException, SQLException
{
	auditDao.refresh(audit);
	if (audit.syncCanceled)
	{
		return;
	}

	JSONObject responseJson = new JSONObject(jsonString).getJSONObject("response");

	final int status = responseJson.getInt("status");
	final int result = responseJson.getInt("result");

	if (status == 1 && result == 1)
	{
		//			route.setServerVersion(routeVersion);
		//			getDbHelper().getRouteDao().update(route);

		JSONObject jsonData = responseJson.getJSONObject("data");
		JSONObject jsonRoute = jsonData.getJSONObject("route");
		JSONObject jsonTtNode = jsonRoute.getJSONObject("tt");

		Iterator ttIterator = jsonTtNode.keys();
		while (ttIterator.hasNext())
		{
			JSONObject jsonTt = jsonTtNode.getJSONObject(ttIterator.next().toString());

			final int respOutletId = jsonTt.getInt("id");
			Outlet outlet = outletDao.queryForId(respOutletId);
			outletDao.createOrUpdate(outlet);

			JSONArray jsonPoints = jsonTt.getJSONArray("stats");
			for (int i = 0; i < jsonPoints.length(); i++)
			{
				JSONObject jsonMonth = jsonPoints.getJSONObject(i);
				Month month = Month.fromCode(jsonMonth.getInt("month"));
				Integer firstPoints = jsonMonth.getInt("first_points");
				Integer secondPoints = jsonMonth.getInt("second_points");
				Integer totalPoints = jsonMonth.getInt("first_points");
				Boolean prize = (jsonMonth.getInt("prize") != 0);

				for (MonthPoints points : monthPointsDao.query(monthPointsDao.queryBuilder()
				                                                             .where()
				                                                             .eq(MonthPoints.OUTLET_COLUMN_NAME, outlet)
				                                                             .prepare()))
				{
					monthPointsDao.delete(points);
				}

				MonthPoints monthPoints = new MonthPoints();
				monthPoints.outlet = outlet;
				monthPoints.month = month;
				monthPoints.firstPoints = firstPoints;
				monthPoints.secondPoints = secondPoints;
				monthPoints.totalPoints = totalPoints;
				monthPoints.prizeGiven = prize;
				monthPointsDao.createOrUpdate(monthPoints);
			}
		}

		audit.synced = true;
		audit.syncFailureCount = 0;
		audit.lastSynced = new Date();
		auditDao.update(audit);

		dispatchAuditSyncedEvent(audit);
		dispatchDataSyncedEvent();
	}
	else
	{
		audit.syncFailed = true;
		audit.syncFailureCount += 1;
		audit.lastSynced = new Date();
		auditDao.update(audit);
	}
}

private void synchronizeAudit(Audit audit) throws JSONException, SQLException
{
	try
	{
		final Session session = resumeSession();
		final Route route = resumeRoute();

		audit.syncCanceled = false;
		auditDao.update(audit);

		final JSONObject jsonObject = createRequestJson(session, route, audit);

		HttpRequest httpRequest = new HttpRequest();
		String responseString = httpRequest.sendJSONPost("http://api.s1program.ru/", jsonObject);

		updateAuditFromJsonString(audit, responseString);
	}
	catch (JSONException e)
	{
		Log.e(getClass().getName(), "Ошибка синхронизации аудита", e);

		audit.syncFailed = true;
		audit.syncFailureCount += 1;
		audit.lastSynced = new Date();
		auditDao.update(audit);
	}
	catch (IOException e)
	{
		Log.e(getClass().getName(), "Ошибка синхронизации аудита", e);

		audit.syncFailed = true;
		audit.syncFailureCount += 1;
		audit.lastSynced = new Date();
		auditDao.update(audit);
	}
	catch (SQLException e)
	{
		Log.e(getClass().getName(), "Ошибка синхронизации аудита", e);

		audit.syncFailed = true;
		audit.syncFailureCount += 1;
		audit.lastSynced = new Date();
		auditDao.update(audit);
	}
}

private void synchronizeAuditPhoto(AuditPhoto auditPhoto)
{
	if (auditPhoto.path == null)
	{
		return;
	}

	Session session = resumeSession();
	final Route route = resumeRoute();
	FileUploader uploader = new FileUploader();

	try
	{
		Log.d(getClass().getName(), "Отправка файла " + auditPhoto.path);

		Dao<Audit, Integer> auditDao = getDbHelper().getAuditDao();
		Dao<Outlet, Integer> outletDao = getDbHelper().getOutletDao();
		Dao<AuditPhoto, Integer> auditPhotoDao = getDbHelper().getAuditPhotoDao();

		final Audit audit = auditPhoto.audit;
		auditDao.refresh(audit);

		final Outlet outlet = audit.outlet;
		outletDao.refresh(outlet);


		String urlWithParams = "http://api.s1program.ru/upload/?uid=" + session.uid + "&sid=" + session.sid + "&route=" + route.serverId + "&tt=" + outlet.id + "&kind=0";
		String responseString = uploader.executeMultipartPost(urlWithParams, auditPhoto.path, auditPhoto.filename);
		JSONObject responseJson = new JSONObject(responseString).getJSONObject("response");
		if (responseJson.getInt("status") == 1 && responseJson.getInt("result") == 1)
		{
			auditPhoto.synced = true;
			auditPhoto.syncFailed = false;
			auditPhoto.lastSynced = new Date();
			auditPhotoDao.update(auditPhoto);
			auditPhotoDao.refresh(auditPhoto);
			auditDao.update(audit);
			Log.d(getClass().getName(),
			      "refreshed auditPhoto: " + auditPhoto.id + ", " + auditPhoto.synced + ", " + auditPhoto.syncFailed);

			dispatchAuditPhotoSyncedEvent(auditPhoto);
			dispatchDataSyncedEvent();
		}
		else
		{
			auditPhoto.synced = false;
			auditPhoto.syncFailed = true;
			auditPhoto.syncFailureCount += 1;
			auditPhoto.lastSynced = new Date();
			Log.d(getClass().getName(), "Помечаем, что отгрузка фото аудита не удалась.");
			auditPhotoDao.update(auditPhoto);
			auditPhotoDao.refresh(auditPhoto);
			auditDao.update(audit);
			Log.d(getClass().getName(),
			      "refreshed auditPhoto: " + auditPhoto.id + ", " + auditPhoto.synced + ", " + auditPhoto.syncFailed);
		}

		Log.d(getClass().getName(), "Ответ: " + responseString);
	}
	catch (IOException e)
	{
		Log.e(getClass().getName(), "Ошибка отправки файла", e);

		auditPhoto.synced = false;
		auditPhoto.syncFailed = true;
		auditPhoto.syncFailureCount += 1;
		auditPhoto.lastSynced = new Date();
	}
	catch (JSONException e)
	{
		Log.e(getClass().getName(), "Ошибка синхронизации аудита", e);

		auditPhoto.synced = false;
		auditPhoto.syncFailed = true;
		auditPhoto.syncFailureCount += 1;
		auditPhoto.lastSynced = new Date();
	}
	catch (Exception e)
	{
		Log.e(getClass().getName(), "Ошибка синхронизации аудита", e);

		auditPhoto.synced = false;
		auditPhoto.syncFailed = true;
		auditPhoto.syncFailureCount += 1;
		auditPhoto.lastSynced = new Date();
	}
}

private void synchronizeUnitPhoto(UnitPhoto unitPhoto) throws SQLException
{
	if (unitPhoto.path == null)
	{
		return;
	}

	Session session = resumeSession();
	final Route route = resumeRoute();

	FileUploader uploader = new FileUploader();
	try
	{
		Log.d(getClass().getName(), "Отправка файла " + unitPhoto.path);

		Dao<Audit, Integer> auditDao = getDbHelper().getAuditDao();
		auditDao.refresh(unitPhoto.audit);
		Dao<Outlet, Integer> outletDao = getDbHelper().getOutletDao();
		outletDao.refresh(unitPhoto.audit.outlet);
		String urlWithParams = "http://api.s1program.ru/upload/?uid=" + session.uid + "&sid=" + session.sid + "&route=" + route.serverId + "&tt=" + unitPhoto.audit.outlet.id + "&kind=" + unitPhoto
				                                                                                                                                                                                   .kind
				                                                                                                                                                                                   .getCode();
		String responseString = uploader.executeMultipartPost(urlWithParams, unitPhoto.path, unitPhoto.filename);
		JSONObject responseJson = new JSONObject(responseString).getJSONObject("response");
		if (responseJson.getInt("status") == 1 && responseJson.getInt("result") == 1)
		{
			unitPhoto.synced = true;
			unitPhoto.syncFailed = false;
			unitPhoto.lastSynced = new Date();
			unitPhotoDao.update(unitPhoto);

			dispatchUnitPhotoSyncedEvent(unitPhoto);
			dispatchDataSyncedEvent();
		}
		else
		{
			unitPhoto.synced = false;
			unitPhoto.syncFailed = true;
			unitPhoto.syncFailureCount += 1;
			unitPhoto.lastSynced = new Date();
			unitPhotoDao.update(unitPhoto);
		}
	}
	catch (IOException e)
	{
		unitPhoto.synced = false;
		unitPhoto.syncFailed = true;
		unitPhoto.syncFailureCount += 1;
		unitPhoto.lastSynced = new Date();
		unitPhotoDao.update(unitPhoto);

		Log.e(getClass().getName(), "Ошибка отправки файла", e);
	}
	catch (JSONException e)
	{
		unitPhoto.synced = false;
		unitPhoto.syncFailed = true;
		unitPhoto.syncFailureCount += 1;
		unitPhoto.lastSynced = new Date();
		unitPhotoDao.update(unitPhoto);

		Log.e(getClass().getName(), "Ошибка синхронизации аудита", e);
	}
	catch (Exception e)
	{
		unitPhoto.synced = false;
		unitPhoto.syncFailed = true;
		unitPhoto.syncFailureCount += 1;
		unitPhoto.lastSynced = new Date();
		unitPhotoDao.update(unitPhoto);

		Log.e(getClass().getName(), "Ошибка синхронизации аудита", e);
	}
}

// ----------------------------------------------------------------------
// Protected methods
// ----------------------------------------------------------------------

@Override
protected void onHandleIntent(Intent intent)
{
	cancelAlarm();

	doTick();

	prepareNextAlarm();
}

// ----------------------------------------------------------------------
// Public methods
// ----------------------------------------------------------------------

@Override
public void onCreate()
{
	super.onCreate();

	initializeDaoFields();
}

@Override
public void onDestroy()
{
	super.onDestroy();

	releaseDbHelper();
}

}

