package ru.s1program.tripleStrike.orm;

import com.xtremelabs.robolectric.RobolectricTestRunner;
import com.xtremelabs.robolectric.util.DatabaseConfig;
import com.xtremelabs.robolectric.util.SQLiteMap;
import org.junit.*;
import org.junit.runner.RunWith;
import ru.s1program.tripleStrike.R;
import ru.s1program.tripleStrike.activities.LoginActivity;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@DatabaseConfig.UsingDatabaseMap(SQLiteMap.class)
@RunWith(RobolectricTestRunner.class)
public class OrmTest
{

@BeforeClass
public static void beforeClass()
{

}

@AfterClass
public static void tearDown()
{

}


@Test
public void shouldHaveHappySmiles() throws Exception
{
	String appName = new LoginActivity().getResources()
	                                    .getString(R.string.app_name);
	assertThat(appName, equalTo("Triple Strike"));
}
}
