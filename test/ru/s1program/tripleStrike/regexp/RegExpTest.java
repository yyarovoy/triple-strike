package ru.s1program.tripleStrike.regexp;

import org.junit.Assert;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegExpTest {

@Test
public void testPhoneFormatting()
{
	Pattern pattern = Pattern.compile("^(89|\\+79)");

	Matcher matcher = pattern.matcher("89122000089");
	Assert.assertEquals("79122000089", matcher.replaceFirst("79"));

	matcher = pattern.matcher("79122000089");
	Assert.assertEquals("79122000089", matcher.replaceFirst("79"));

	matcher = pattern.matcher("+79122000089");
	Assert.assertEquals("79122000089", matcher.replaceFirst("79"));
}

}
