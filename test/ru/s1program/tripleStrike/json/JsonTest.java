package ru.s1program.tripleStrike.json;

import android.util.Log;
import com.xtremelabs.robolectric.RobolectricTestRunner;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.s1program.tripleStrike.net.HttpRequest;

import java.io.IOException;

@RunWith(RobolectricTestRunner.class)
public class JsonTest
{

private JSONObject createLoginJson(String login, String password, String phone) throws JSONException
{
	JSONObject jsonObject = new JSONObject();
	JSONObject data = new JSONObject();
	data.put("login", login);
	data.put("password", password);
	data.put("phone", phone);

	JSONObject request = new JSONObject();
	request.put("version", "1.0");
	request.put("action", "login");
	request.put("data", data);

	jsonObject.put("request", request);
	return jsonObject;
}


@Before
public void before()
{
}

@After
public void after()
{
}

@Test
public void testLoginJson() throws JSONException, IOException
{
	JSONObject jsonObject = createLoginJson("A106021", "234979", "+79162000000");

	HttpRequest httpRequest = new HttpRequest();

	String response = httpRequest.sendJSONPost("http://api.s1program.ru/", jsonObject);

	JSONArray jsonArray = new JSONArray(response);

	Log.d("Test", jsonArray.toString());
}

}
